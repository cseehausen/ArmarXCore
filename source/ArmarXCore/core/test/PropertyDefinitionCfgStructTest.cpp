/**
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::CfgStructPropertiesTest
#define ARMARX_BOOST_TEST

#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <ArmarXCore/core/application/properties/PluginAll.h>

#include <string>
#include <iostream>


using namespace armarx;

enum class enum_class
{
    A, B
};
namespace simox::meta
{
    template<>
    const simox::meta::EnumNames<enum_class>
    enum_names<enum_class>
    {
        { enum_class::A, "A" }, { enum_class::B, "B" }
    };
}

ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(
    armarx, testsubcfg,
    (int, a, AX_DEFAULT(1), AX_PROPERTY_NAME("q.w")),
    (int, b, AX_DEFAULT(2))
);

ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(
    armarx, testcfg,
    (
        enum_class, enu,
        AX_DEFAULT(enum_class::A)
    ),
    (armarx::testsubcfg, a),
    (armarx::testsubcfg, b)
);

class TestApp : public Application
{
public:
    void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override {}

    ArmarXManagerPtr getArmarXManager()
    {
        return Application::getArmarXManager();
    }
};

class TestComponent : public Component
{
public:
    void onInitComponent() override {}
    void onConnectComponent() override {}
    std::string getDefaultName() const override
    {
        return "TestComponent";
    }

    testcfg cfg1;
    testcfg cfg2;

    armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
    {
        armarx::PropertyDefinitionsPtr ptr =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());
        ptr->optional(cfg1, "", "");
        ptr->optional(cfg2, "pre", "");
        return ptr;
    }
};

BOOST_AUTO_TEST_CASE(CfgStructPropertiesTest)
{
    std::cout << "creating a manager\n";
    IceTestHelper iceTestHelper;
    iceTestHelper.startEnvironment();
    TestArmarXManagerPtr manager = new TestArmarXManager(
        "CfgStructPropertiesTest",
        iceTestHelper.getCommunicator());
    std::cout << "creating a manager...DONE!\n";

    auto comp = Component::create<TestComponent>();
    manager->addObject(comp, std::string {"Obj1"}, false, false);
    comp->getObjectScheduler()->waitForObjectStateMinimum(armarx::eManagedIceObjectStarted);

    BOOST_CHECK_EQUAL(comp->getProperty<int>("a.q.w").getValue(), 1);
    BOOST_CHECK_EQUAL(comp->getProperty<int>("pre.a.b").getValue(), 2);

    manager->removeObjectBlocking(comp);
    manager->shutdown();
}
