#include "Decoupled.h"

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/ManagedIceObjectRegistryInterface.h>

namespace armarx
{

    static CreateComponentFunction* global_createComponent;
    static std::string global_name;

    bool Decoupled::setCreateComponentFunction(CreateComponentFunction* function,
            std::string const& name)
    {
        global_createComponent = function;
        global_name = name;
        return global_createComponent != nullptr;
    }

    struct DecoupledSingleComponentApp : armarx::Application
    {
        std::string appConfigName;
        std::string appConfigDomain;

        void setup(const armarx::ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties) override
        {
            registry->addObject(global_createComponent(properties, "", appConfigDomain));
        }
    };

    int DecoupledMain(int argc, char* argv[])
    {
        std::string configName = "";
        std::string configDomain = "ArmarX";
        bool enableLibLoading = false;

        IceUtil::Handle<DecoupledSingleComponentApp> app(new DecoupledSingleComponentApp);
        armarx::Application::setInstance(app);

        app->appConfigDomain = configDomain;
        app->appConfigName = configName;
        app->enableLibLoading(enableLibLoading);
        app->setName(global_name);
        app->storeCommandLineArguments(argc, argv);
        return app->main(argc, argv);
    }

}
