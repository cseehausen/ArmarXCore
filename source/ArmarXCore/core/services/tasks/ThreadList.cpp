/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ThreadList.h"
#include "PeriodicTask.h"
#include "../../system/ProcessWatcher.h"

#include <mutex>
#include <set>

namespace armarx
{
    struct ThreadList::Impl
    {
        std::set<RunningTaskIceBase*> activeRunningTaskList;
        std::set<PeriodicTaskIceBase*> activePeriodicTaskList;
        std::mutex runListMutex;
        std::mutex periodicListMutex;
        std::mutex procTotalTimeMutex;

        std::shared_ptr<ProcessWatcher> processWatcher;
    };

    ThreadList::ThreadList()
        : impl(new Impl)
    {
        impl->processWatcher.reset(new ProcessWatcher(getProfiler()));
    }

    ThreadList::~ThreadList()
    {

    }

    void ThreadList::onInitComponent()
    {
    }

    void ThreadList::onConnectComponent()
    {
        impl->processWatcher->start();
    }

    void ThreadList::onDisconnectComponent()
    {
        impl->processWatcher->stop();
    }

    void ThreadList::updateCPUUsage()
    {
    }

    Ice::StringSeq ThreadList::getRunningTaskNames(const Ice::Current& c)
    {
        std::unique_lock lock(impl->runListMutex);
        Ice::StringSeq result;

        for (std::set<RunningTaskIceBase*> ::const_iterator it = impl->activeRunningTaskList.begin(); it != impl->activeRunningTaskList.end(); ++it)
        {
            result.push_back((*it)->name);
        }

        return result;
    }

    Ice::StringSeq ThreadList::getPeriodicTaskNames(const Ice::Current& c)
    {
        std::unique_lock lock(impl->periodicListMutex);

        Ice::StringSeq result;

        for (std::set<PeriodicTaskIceBase*>::const_iterator it = impl->activePeriodicTaskList.begin(); it != impl->activePeriodicTaskList.end(); ++it)
        {
            result.push_back((*it)->name);
        }

        return result;
    }

    double ThreadList::getCpuUsage(const Ice::Current& c)
    {
        std::unique_lock lock(impl->procTotalTimeMutex);
        return impl->processWatcher->getProcessCpuUsage().proc_total_time;
    }

    RunningTaskList ThreadList::getRunningTasks(const Ice::Current& c)
    {
        std::unique_lock lock(impl->runListMutex);
        RunningTaskList resultList;
        std::set<RunningTaskIceBase*>::const_iterator it =  impl->activeRunningTaskList.begin();

        for (; it != impl->activeRunningTaskList.end(); it++)
        {
            resultList.push_back(**it);
            resultList.rbegin()->workload = impl->processWatcher->getThreadLoad((*it)->threadId);
        }

        return resultList;
    }

    PeriodicTaskList ThreadList::getPeriodicTasks(const Ice::Current& c)
    {
        std::unique_lock lock(impl->periodicListMutex);
        PeriodicTaskList resultList;
        std::set<PeriodicTaskIceBase*>::const_iterator it =  impl->activePeriodicTaskList.begin();

        for (; it != impl->activePeriodicTaskList.end(); it++)
        {
            resultList.push_back(**it);
        }

        return resultList;
    }


    void ThreadList::addRunningTask(RunningTaskIceBase* threadPtr)
    {
        std::unique_lock lock(impl->runListMutex);
        impl->processWatcher->addThread(threadPtr->threadId);

        if (impl->activeRunningTaskList.find(threadPtr) != impl->activeRunningTaskList.end())
        {
            return;
        }

        impl->activeRunningTaskList.insert(threadPtr);
    }

    bool ThreadList::removeRunningTask(RunningTaskIceBase* threadPtr)
    {
        std::unique_lock lock(impl->runListMutex);
        impl->processWatcher->removeThread(threadPtr->threadId);

        if (impl->activeRunningTaskList.find(threadPtr) == impl->activeRunningTaskList.end())
        {
            return false;
        }

        impl->activeRunningTaskList.erase(threadPtr);
        return true;
    }

    void ThreadList::addPeriodicTask(PeriodicTaskIceBase* threadPtr)
    {
        std::unique_lock lock(impl->periodicListMutex);

        if (impl->activePeriodicTaskList.find(threadPtr) != impl->activePeriodicTaskList.end())
        {
            return;
        }

        impl->activePeriodicTaskList.insert(threadPtr);
    }

    bool ThreadList::removePeriodicTask(PeriodicTaskIceBase* threadPtr)
    {
        std::unique_lock lock(impl->periodicListMutex);

        if (impl->activePeriodicTaskList.find(threadPtr) == impl->activePeriodicTaskList.end())
        {
            return false;
        }

        impl->activePeriodicTaskList.erase(threadPtr);
        return true;
    }

    ThreadListPtr ThreadList::getApplicationThreadList()
    {
        static ThreadListPtr applicationThreadList = new ThreadList();
        return applicationThreadList;
    }

    void ThreadList::setApplicationThreadListName(const std::string& threadListName)
    {
        getApplicationThreadList()->setName(threadListName);
    }
}

std::ostream& std::operator<<(std::ostream& stream, const armarx::RunningTaskIceBase& task)
{
    stream << "Threadname: " << task.name << " \n ";
    stream << "\tWorldload: " << task.workload << " \n ";
    stream << "\tRunning: " << task.running << " \n ";
    stream << "\tStartTime: " << IceUtil::Time::microSeconds(task.startTime).toDateTime() << " \n ";
    stream << "\tLastFeedbackTime: " << IceUtil::Time::microSeconds(task.lastFeedbackTime).toDateTime() << " \n ";
    stream << "\tStopped: " << task.stopped << " \n ";
    stream << "\tFinished: " << task.finished << "";

    return stream;
}
