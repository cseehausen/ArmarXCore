/**

\defgroup ArmarXCore ArmarXCore

\defgroup core-utility Utility Features
\section ArmarXCore-BasicTypes Basic Data Types
As a software developer your software consists usually deep down of the basic data types: bool, int, float, double, string.
These exists of course also in the IDL Slice that we use.
Additionally, you will frequently need containers of these basic types and for less typing typedefs onto these.
These are also already defined in Ice in the case of a list and there is no need to redefine these typedefs again.

The scheme they are defined in is: &lt;type&gt;Seq.

So for example *IntSeq*.

They are defined in &lt;Ice/BuiltinSequences.ice&gt; resp. &lt;Ice/BuiltinSequences.h&gt;.

The complete list can be found here: https://doc.zeroc.com/display/Ice35/Ice+Slice+API#IceSliceAPI-StringSeq

Additionally, ArmarX provides a concept for transmitting custom types over one basic type: The \ref VariantsGrp.
This is an essential part of the generic \ref Observers "Observer concept" and the \ref Statechart "Statecharts".


\defgroup core-slice Slice Documentation
\ingroup core-utility
- \subpage slicedocumentation


\page armarx-extended-doc Extended Documentation

Organization of a project is an important and challenging task.
Therefore the following pages describe the most importants aspect
of the organizational structure of ArmarX.

- \subpage execution
- \subpage structure
- \subpage cmake
- \subpage cmake-internals
- \subpage documentation
- \subpage armarx-componentproperties
- \subpage conventions
- \subpage git
- \subpage testing
- \subpage versioning
- \subpage ice


*/
