#pragma once

#include <SimoxUtility/meta/type_traits/is_any_of.h>

#include "common.h"
#include "config_struct_details.h"

//possible details
namespace armarx::meta::cfg
{
    template<class CL, class MT, MT CL::* ptr>
    constexpr undefined_t element_min = {};

    template<class CL, class MT, MT CL::* ptr>
    constexpr undefined_t element_max = {};

    template<class CL, class MT, MT CL::* ptr>
    constexpr undefined_t element_description = {};

    template<class CL, class MT, MT CL::* ptr>
    constexpr undefined_t element_property_name = {};

    template<class CL, class MT, MT CL::* ptr>
    constexpr undefined_t element_decimals = {};

    template<class CL, class MT, MT CL::* ptr>
    constexpr undefined_t element_steps = {};

    template<class CL, class MT, MT CL::* ptr>
    constexpr undefined_t element_label = {};

    template<class CL, class MT, MT CL::* ptr>
    constexpr std::false_type element_no_prop = {};

    template<class CL, class MT, MT CL::* ptr>
    constexpr std::false_type element_no_rem_gui = {};

    template<class CL, class MT, MT CL::* ptr>
    struct element_default : std::false_type
    {
        static const MT& get()
        {
            return struct_default<CL>::get().*ptr;
        }
        static void set(CL& cl)
        {
            cl.*ptr = get();
        }
    };
}

//collection of details + check of validity
namespace armarx::meta::cfg
{
    template<class CL, class MT, MT CL::* ptr>
    struct element_details
    {
        using class_t = CL;
        using member_t = MT;
        static constexpr auto member_ptr    = ptr;
        static constexpr auto member_name   = hana_member_name     <CL, MT, ptr>;

        static constexpr auto min           = element_min          <CL, MT, ptr>;
        static constexpr auto max           = element_max          <CL, MT, ptr>;
        static constexpr auto description   = element_description  <CL, MT, ptr>;
        static constexpr auto property_name = element_property_name<CL, MT, ptr>;
        static constexpr auto decimals      = element_decimals     <CL, MT, ptr>;
        static constexpr auto steps         = element_steps        <CL, MT, ptr>;
        static constexpr auto label         = element_label        <CL, MT, ptr>;

        static constexpr bool is_no_prop    = decltype(element_no_prop<CL, MT, ptr>)::value;
        static constexpr bool no_remote_gui = decltype(element_no_rem_gui<CL, MT, ptr>)::value;

        using min_t           = decltype(min);
        using max_t           = decltype(max);
        using description_t   = decltype(description);
        using property_name_t = decltype(property_name);
        using decimals_t      = decltype(decimals);
        using steps_t         = decltype(steps);
        using label_t         = decltype(label);
        using widget_t        = element_widget<CL, MT, ptr>;

        static std::string description_as_string()
        {
            if constexpr(is_not_undefined_t(description))
            {
                return description;
            }
            return "";
        }

        static std::string make_property_name(std::string pre)
        {
            if (!pre.empty())
            {
                pre += '.';
            }
            if constexpr(is_not_undefined_t(property_name))
            {
                return pre + property_name;
            }
            return pre + member_name;
        }

        //general checks (does not check details for gui)
        static constexpr bool assert_settings()
        {
            //check label / description
            static_assert(undefined_t_or_type<const char*>(label), "if set, element_label has to be of type const char*");
            static_assert(undefined_t_or_type<const char*>(description), "if set, element_description has to be of type const char*");
            static_assert(undefined_t_or_type<const char*>(property_name), "if set, element_property_name has to be of type const char*");

            //check min / max / steps / decimals
            if constexpr(simox::meta::is_any_of_v<MT, std::string, bool>)
            {
                static_assert(is_undefined_t(min), "element_min can't be set for std::string or bool");
                static_assert(is_undefined_t(max), "element_max can't be set for std::string or bool");
                static_assert(is_undefined_t(steps), "element_steps can't be set for std::string or bool");
                static_assert(is_undefined_t(decimals), "element_decimals can't be set for std::string or bool");
            }
            else if constexpr(std::is_integral_v<MT>)
            {
                static_assert(undefined_t_or_type_or_array_sz_1<MT>(min), "if set, element_min has to be of the same type as the element");
                static_assert(undefined_t_or_type_or_array_sz_1<MT>(max), "if set, element_max has to be of the same type as the element");

                static_assert(undefined_t_or_type_or_array_sz_1<int>(steps), "if set, element_steps has to be of type int");
                static_assert(is_undefined_t(decimals), "element_decimals can't be set for integral types");
            }
            else if constexpr(std::is_floating_point_v<MT>)
            {
                static_assert(undefined_t_or_type_or_array_sz_1<MT>(min), "if set, element_min has to be of the same type as the element");
                static_assert(undefined_t_or_type_or_array_sz_1<MT>(max), "if set, element_max has to be of the same type as the element");

                static_assert(undefined_t_or_type_or_array_sz_1<int>(steps),    "if set, element_steps has to be of type int");
                static_assert(undefined_t_or_type_or_array_sz_1<int>(decimals), "if set, element_decimals has to be of type int");
            }
            else
            {
                ///TODO MX / VEC

                static_assert(is_undefined_t(min), "element_min can't be set for this type");
                static_assert(is_undefined_t(max), "element_max can't be set for this type");
                static_assert(is_undefined_t(steps), "element_steps can't be set for this type");
                static_assert(is_undefined_t(decimals), "element_decimals can't be set for this type");
            }
            return true;
        }
        static_assert(assert_settings());
    };

    template<class VarName, class CL, class MT, MT CL::* ptr>
    element_details<CL, MT, ptr>
    to_element_detail(
        const boost::hana::pair<VarName, boost::hana::struct_detail::member_ptr<MT CL::*, ptr>>&
    );
}
