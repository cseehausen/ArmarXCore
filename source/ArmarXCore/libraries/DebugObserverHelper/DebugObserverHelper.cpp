/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::DebugObserverHelper
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "DebugObserverHelper.h"

namespace armarx::detail
{
    std::string fix_channel_name(const std::string& channelName)
    {
        std::string result = channelName;
        for (auto& c : result)
        {
            if (c == '.')
            {
                c = '_';
            }
        }
        return result;
    }
}

namespace armarx
{

    void DebugObserverHelper::setFloatFallbackForBigTooLargeInts(bool useFallback)
    {
        _float_fallback_for_big_too_large_ints = useFallback;
    }
    DebugObserverHelper::DebugObserverHelper::DebugObserverHelper(const DebugObserverInterfacePrx& prx, bool batchmode) :
        _observer{prx}, _batchMode{batchmode}
    {}

    void DebugObserverHelper::setDebugObserverBatchModeEnabled(bool enable)
    {
        _batchMode = enable;
    }
    void DebugObserverHelper::sendDebugObserverBatch()
    {
        ARMARX_CHECK_NOT_NULL(_observer);
        for (const auto& [chname, data] : _batch)
        {
            _observer->setDebugChannel(chname, data);
        }
        _batch.clear();
    }

    void DebugObserverHelper::setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const TimedVariantPtr& value) const
    {
        ARMARX_CHECK_NOT_NULL(value);
        const auto fixedChName = detail::fix_channel_name(channelName);
        if (_batchMode)
        {
            _batch[fixedChName][datafieldName] = value;
        }
        else
        {
            ARMARX_CHECK_NOT_NULL(_observer);
            _observer->setDebugDatafield(fixedChName, datafieldName, value);
        }
    }
    void DebugObserverHelper::setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName,  const VariantPtr& value) const
    {
        const auto fixedChName = detail::fix_channel_name(channelName);
        setDebugObserverDatafield(fixedChName, datafieldName, IceUtil::Time::now(), value);
    }
    void DebugObserverHelper::setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName,  const VariantBasePtr& value) const
    {
        const auto fixedChName = detail::fix_channel_name(channelName);
        ARMARX_CHECK_NOT_NULL(value);
        if (const auto ptr = TimedVariantPtr::dynamicCast(value); ptr)
        {
            setDebugObserverDatafield(fixedChName, datafieldName, ptr);
        }
        const auto ptr = VariantPtr::dynamicCast(value);
        ARMARX_CHECK_NOT_NULL(ptr) << "not derived from Variant";
        setDebugObserverDatafield(fixedChName, datafieldName, ptr);
    }

    void DebugObserverHelper::setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const IceUtil::Time& time, const VariantPtr& value) const
    {
        setDebugObserverDatafield(detail::fix_channel_name(channelName), datafieldName, TimedVariantPtr{new TimedVariant(value, time)});
    }
    void DebugObserverHelper::setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const IceUtil::Time& time, const Variant& value) const
    {
        setDebugObserverDatafield(detail::fix_channel_name(channelName), datafieldName, TimedVariantPtr{new TimedVariant(value, time)});
    }


    void DebugObserverHelper::setDebugObserverChannel(const std::string& channelName, StringVariantBaseMap valueMap) const
    {
        ARMARX_CHECK_NOT_NULL(_observer);
        _observer->setDebugChannel(detail::fix_channel_name(channelName), valueMap);
    }

    void DebugObserverHelper::removeDebugObserverChannel(const std::string& channelname) const
    {
        ARMARX_CHECK_NOT_NULL(_observer);
        _observer->removeDebugChannel(detail::fix_channel_name(channelname));
    }

    void DebugObserverHelper::removeDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName) const
    {
        ARMARX_CHECK_NOT_NULL(_observer);
        _observer->removeDebugDatafield(detail::fix_channel_name(channelName), datafieldName);
    }

    void DebugObserverHelper::removeAllDebugObserverChannels() const
    {
        ARMARX_CHECK_NOT_NULL(_observer);
        _observer->removeAllChannels();
    }

    const DebugObserverInterfacePrx& DebugObserverHelper::getDebugObserver() const
    {
        return _observer;
    }
    void DebugObserverHelper::setDebugObserver(const DebugObserverInterfacePrx& prx)
    {
        _observer = prx;
    }

    const std::string& DebugObserverHelper::getChannelName() const
    {
        return _channelName;
    }

    void DebugObserverHelper::setChannelName(const std::string& name)
    {
        _channelName = name;
    }

    void DebugObserverHelper::setChannelName(const ManagedIceObject& obj)
    {
        setChannelName(obj.getName());
    }

    void DebugObserverHelper::setChannelName(const ManagedIceObject* obj)
    {
        ARMARX_CHECK_NOT_NULL(obj);
        setChannelName(*obj);
    }

    void DebugObserverHelper::setChannelName(const ManagedIceObjectPlugin& obj)
    {
        setChannelName(obj.parent());
    }

    void DebugObserverHelper::setChannelName(const ManagedIceObjectPlugin* obj)
    {
        ARMARX_CHECK_NOT_NULL(obj);
        setChannelName(*obj);
    }
}
