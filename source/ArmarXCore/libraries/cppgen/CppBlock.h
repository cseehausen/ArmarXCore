/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <vector>
#include <string>
#include <memory>
#include "CppWriter.h"
#include <boost/format/format_fwd.hpp>

namespace armarx
{
    class CppBlock;
    using CppBlockPtr = std::shared_ptr<CppBlock>;

    class CppBlockEntry;
    using CppBlockEntryPtr = std::shared_ptr<CppBlockEntry>;

    class CppBlockEntry
    {
    public:
        CppBlockEntry() = default;
        virtual void writeCpp(CppWriterPtr writer) = 0;
        virtual std::string getAsSingleLine() = 0;

    };

    class CppBlockStringEntry :
        virtual public CppBlockEntry
    {
    public:
        CppBlockStringEntry(const std::string&);
        virtual void writeCpp(CppWriterPtr writer) override;
        virtual std::string getAsSingleLine() override;

    private:
        std::string line;
    };

    class CppBlockBlockEntry :
        virtual public CppBlockEntry
    {
    public:
        CppBlockBlockEntry(const CppBlockPtr&);
        virtual void writeCpp(CppWriterPtr writer) override;
        virtual std::string getAsSingleLine() override;

    private:
        CppBlockPtr nestedBlock;
    };

    class CppBlock
    {
    public:
        CppBlock();

        void writeCpp(CppWriterPtr writer);
        std::string getAsSingleLine();
        void addLine(const std::string& line);
        void addLine(const boost::basic_format<char>& line);

        void addCommentLine(const std::string& line);
        void addCommentLine(const boost::basic_format<char>& line);

        void addCommentLines(const std::vector<std::string>& lines);
        void addCommentLines(const std::vector<boost::basic_format<char>>& lines);

        void addBlock(const CppBlockPtr& block);
        void appendBlock(const CppBlockPtr& block);

        static CppBlockPtr MergeBlocks(const CppBlockPtr& block1, const CppBlockPtr& block2);

    protected:
        void addEntry(const CppBlockEntryPtr& entry);
        std::vector<CppBlockEntryPtr> entries;
    };
}

