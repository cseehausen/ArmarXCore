/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <random>

#include "XMLStateComponent.h"
#include "GroupXmlReader.h"
#include "XMLRemoteStateOfferer.h"
#include <IceUtil/UUID.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
#include "../StatechartEventDistributor.h"

#include <Ice/Properties.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

namespace armarx
{

    XMLStateComponent::XMLStateComponent()
    {
        uuid = IceUtil::generateUUID();
    }

    void XMLStateComponent::onInitComponent()
    {
        std::string groupFilepathString = PropertyUser::getProperty<std::string>("XMLStatechartGroupDefinitionFile").getValue();
        std::string profileName = PropertyUser::getProperty<std::string>("XMLStatechartProfile").getValue();

        //std::string profileDefinitionFilepath = PropertyUser::getProperty<std::string>("XMLStatechartProfile").getValue();
        profiles = StatechartProfiles::ReadProfileFiles(Application::getInstance()->getDefaultPackageNames());
        selectedProfile = profiles->getProfileByName(profileName);

        if (!selectedProfile)
        {
            throw LocalException("Could not find profile '" + profileName + "'");
        }

        ARMARX_IMPORTANT << "Using profile " << selectedProfile->getFullName();




        Ice::StringSeq includePaths;
        auto packages = armarx::Application::GetProjectDependencies();
        packages.push_back(Application::GetProjectName());

        for (const std::string& projectName : packages)
        {
            if (projectName.empty())
            {
                continue;
            }

            CMakePackageFinder project(projectName);
            auto pathsString = project.getIncludePaths();
            Ice::StringSeq projectIncludePaths = simox::alg::split(pathsString, ";,");
            includePaths.insert(includePaths.end(), projectIncludePaths.begin(), projectIncludePaths.end());

        }

        StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(selectedProfile));

        ArmarXDataPath::getAbsolutePath(groupFilepathString, groupFilepathString, includePaths);
        reader->readXml(std::filesystem::path(groupFilepathString));

        auto extraProperties = reader->getConfigurationFileContent();
        if (!extraProperties.empty())
        {
            auto props = Ice::createProperties();
            std::filesystem::path temp = std::filesystem::temp_directory_path() / std::to_string(std::random_device{}());
            temp += ".cfg";
            std::ofstream file(temp.string().c_str());
            file << extraProperties;
            file.close();
            props->load(temp.string());
            std::filesystem::remove(temp);
            for (auto& prop : props->getPropertiesForPrefix(""))
            {
                tryAddProperty(prop.first, prop.second);
            }
        }
        CMakePackageFinder pack(reader->getPackageName());

        if (pack.packageFound())
        {
            std::string libPath = "lib" +  reader->getGroupName() + ".so";
            std::string libPaths = pack.getLibraryPaths();
            std::vector<std::string> libpathList = simox::alg::split(libPaths, ";");
            if (!ArmarXDataPath::getAbsolutePath(libPath, libPath, libpathList))
            {
                ARMARX_ERROR << "Could not find state user code lib file: " << libPath;
            }
            else
            {
                loadLib(libPath);
                ArmarXManager::RegisterKnownObjectFactoriesWithIce(getIceManager()->getCommunicator());
            }
        }

        offererName = reader->getGroupName() + STATEOFFERERSUFFIX;
        offerer = XMLStateOffererFactoryBase::fromName(reader->getGroupName() + STATEOFFERERSUFFIX, reader);
        if (!offerer)
        {
            ARMARX_WARNING << "Could not find XMLRemoteStateOfferer with name " << offererName << "\nAvailable classes are:\n" << XMLStateOffererFactoryBase::getAvailableClasses();
            offerer = new XMLRemoteStateOfferer<>(reader);
        }

        if (PropertyUser::getProperty<std::string>("XMLRemoteStateOffererName").isSet())
        {
            offererName = PropertyUser::getProperty<std::string>("XMLRemoteStateOffererName").getValue();
        }
    }

    void XMLStateComponent::onConnectComponent()
    {
        ComponentPtr obj = ComponentPtr::dynamicCast(offerer);
        obj->forceComponentCreatedByComponentCreateFunc();
        StatechartContextPtr::dynamicCast(offerer)->setReportingTopic(getProperty<std::string>("StateReportingTopic").getValue());
        obj->initializeProperties(offererName, getIceProperties(), getConfigDomain());
        if (getProperty<bool>("EnableProfiling").getValue() && !obj->getProperty<bool>("EnableProfiling").isSet())
        {
            obj->enableProfiler(true);
        }

        bool usePrx = false;
        iceObjName = selectedProfile->getStatechartGroupPrefix() + obj->getName();
        if (getArmarXManager()->getIceManager()->isObjectReachable(iceObjName))
        {
            ARMARX_INFO << "Using already running proxy: " << iceObjName;
            usePrx = true;
        }
        else
        {
            getArmarXManager()->addObject(obj, false, iceObjName);
        }

        for (auto state : Split(getProperty<std::string>("StatesToEnter").getValue(), ",", true, true))
        {
            simox::alg::trim(state);
            if (state == "")
            {
                continue;
            }
            createAndEnterInstance(state, usePrx);
        }
    }

    std::string XMLStateComponent::getDefaultName() const
    {
        return "XMLStateComponent";
    }

    PropertyDefinitionsPtr XMLStateComponent::createPropertyDefinitions()
    {
        return new XMLStateComponentProperties(Component::getConfigIdentifier());
    }


    bool XMLStateComponent::loadLib(std::string libPath)
    {
        if (!ArmarXDataPath::getAbsolutePath(libPath, libPath))
        {
            ARMARX_ERROR << "Could not find state user code lib file: " << libPath;
            return false;
        }

        if (libraries.find(libPath) != libraries.end())
        {
            return true;
        }

        try
        {
            DynamicLibraryPtr lib(new DynamicLibrary());
            ARMARX_VERBOSE << "Loading lib: " << libPath;
            lib->load(libPath);
            libraries[libPath] = lib;
        }
        catch (exceptions::local::DynamicLibraryException& e)
        {
            ARMARX_ERROR << "Library loading failed: " << e.what();
            return false;
        }

        return true;
    }

    void XMLStateComponent::createAndEnterInstance(const std::string& stateClassName, bool useExisitingPrx)
    {
        if (useExisitingPrx)
        {
            RemoteStateOffererInterfacePrx off = getArmarXManager()->getIceManager()->getProxy<RemoteStateOffererInterfacePrx>(iceObjName);
            auto stateId = off->createRemoteStateInstance(stateClassName, nullptr, "TopLevel", stateClassName);
            off->callRemoteState(stateId, StringVariantContainerBaseMap());
            stateIDStartedUsingExisitingPrx = stateId;
        }
        else
        {
            StatechartContextPtr context = StatechartContextPtr::dynamicCast(offerer);
            context->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted);
            RemoteStateOffererIceBasePtr off = RemoteStateOffererIceBasePtr::dynamicCast(offerer);
            auto stateId = off->createRemoteStateInstance(stateClassName, nullptr, "TopLevel", stateClassName);
            off->callRemoteState(stateId, StringVariantContainerBaseMap());
        }
    }

    void XMLStateComponent::onDisconnectComponent()
    {
        if (getArmarXManager()->getIceManager()->isObjectReachable(iceObjName))
        {
            RemoteStateOffererInterfacePrx off = getArmarXManager()->getIceManager()->getProxy<RemoteStateOffererInterfacePrx>(iceObjName);
            if (stateIDStartedUsingExisitingPrx != -1 && off->getAvailableStateInstances().count(stateIDStartedUsingExisitingPrx) != 0)
            {
                ARMARX_DEBUG << "Exiting state with id: " << stateIDStartedUsingExisitingPrx << " which has been started using already running proxy.";
                off->breakActiveSubstateRemotely(stateIDStartedUsingExisitingPrx, nullptr);
                off->breakRemoteState(stateIDStartedUsingExisitingPrx, nullptr);
                off->removeInstance(stateIDStartedUsingExisitingPrx);
            }
        }
    }
}



