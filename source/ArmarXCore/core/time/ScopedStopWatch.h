/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2019, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <functional>

// Ice
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/time/StopWatch.h>
#include <ArmarXCore/core/time/TimeUtil.h>


namespace armarx
{
    /**
     * @brief Measures the time this stop watch was inside the current scope.  Takes a lambda as
     * construction parameter (taking an `IceUtil::Time` as parameter), which will be called at
     * destruction (i.e., when the scope was left).
     *
     * Code example:
     *
     * ```cpp
     * {
     *     ScopedStopWatch sw{[](IceUtil::Time duration) {
     *         std::cout << "Operation took " << duration.toMilliSeconds() << " ms";
     *     }};
     *
     *     long_operation();
     * }
     * ```
     */
    class ScopedStopWatch: public StopWatch
    {

    public:

        /**
         * @brief Constructs a `ScopedStopWatch`.
         * @param callback Callback lambda which will be called at destruction with measured time.
         * @param timeMode Time mode, uses system time as default.
         */
        ScopedStopWatch(
            std::function<void(IceUtil::Time)> callback,
            armarx::TimeMode timeMode = armarx::TimeMode::SystemTime
        );

        /**
         * @brief Destructs the `ScopedStopWatch`.
         */
        virtual ~ScopedStopWatch() override;

    private:

        std::function<void(IceUtil::Time)> m_callback;

    };
}
