/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::ArmarXObjects::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <pcl/point_types.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>


namespace armarx
{
    /**
     * @defgroup Component-@COMPONENT_NAME@ @COMPONENT_NAME@
     * @ingroup @PACKAGE_NAME@-Components
     * A description of the component @COMPONENT_NAME@.
     *
     * @class @COMPONENT_NAME@
     * @ingroup Component-@COMPONENT_NAME@
     * @brief Brief description of class @COMPONENT_NAME@.
     *
     * Detailed description of class @COMPONENT_NAME@.
     */
    class @COMPONENT_NAME@ :
        virtual public visionx::PointCloudProcessor
    {
        /// The used point type.
        using PointT = pcl::PointXYZRGBA;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::process()
        void process() override;


    private:

        DebugObserverInterfacePrx debugObserver;

    };
}

