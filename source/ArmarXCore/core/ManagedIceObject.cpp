/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/interface/core/Profiler.h>
#include "ArmarXCore/core/ArmarXFwd.h"  // for ArmarXObjectSchedulerPtr, etc
#include "ArmarXCore/core/exceptions/local/ExpressionException.h"
#include "ArmarXCore/core/logging/LogSender.h"  // for LogSender
#include "ArmarXCore/core/logging/Logging.h"  // for ARMARX_VERBOSE, etc
#include "ArmarXCore/core/services/profiler/Profiler.h"  // for Profiler, etc
#include "ArmarXCore/interface/core/ManagedIceObjectDefinitions.h"
#include "ArmarXCore/interface/core/ManagedIceObjectDependencyBase.h"
#include "ArmarXCore/core/services/tasks/TaskUtil.h"
#include "ArmarXManager.h"              // for ArmarXObjectSchedulerPtr, etc
#include "ArmarXObjectScheduler.h"      // for ArmarXObjectScheduler
#include "IceManager.h"                 // for IceManager, IceManagerPtr
#include "ManagedIceObject.h"
#include "ManagedIceObjectImpl.h"
#include "ManagedIceObjectDependency.h"
#include "services/profiler/IceLoggingStrategy.h"
#include "services/profiler/LoggingStrategy.h"  // for LoggingStrategy, etc
#include <ArmarXCore/util/CPPUtility/GetTypeString.h>

#include <Ice/BuiltinSequences.h>       // for StringSeq
#include <Ice/Identity.h>               // for Identity
#include <Ice/LocalException.h>
#include <Ice/ObjectAdapter.h>          // for ObjectAdapterPtr, etc
#include <IceStorm/IceStorm.h>          // for TopicPrx
#include <IceUtil/Shared.h>             // for Shared
#include <IceUtil/UUID.h>               // for generateUUID

#include <algorithm>                    // for find
#include <map>                          // for _Rb_tree_iterator, etc
#include <ostream>                      // for operator<<, basic_ostream, etc
#include <utility>                      // for pair, make_pair
#include <sstream>

namespace Ice
{
    class NotRegisteredException;
    class ObjectAdapterDeactivatedException;
}  // namespace Ice


namespace armarx
{
    // *******************************************************
    // static data
    // *******************************************************
    const ManagedIceObjectPtr ManagedIceObject::NullPtr(nullptr);

    // *******************************************************
    // construction
    // *******************************************************
    ManagedIceObject::ManagedIceObject()
        : impl(new Impl)
    {
        impl->name = "";
        impl->objectState = eManagedIceObjectCreated;
        impl->profiler.reset(new Profiler::Profiler());
        impl->enableProfilerFunction = &ManagedIceObject::Noop;
    }

    std::unique_ptr<ManagedIceObjectPlugin>& ManagedIceObject::getPluginPointer(std::type_info const& type, std::string const& prefix)
    {
        ARMARX_CHECK_EXPRESSION(getState() == ManagedIceObjectState::eManagedIceObjectCreated);
        return _plugins[ {type, prefix}];
    }

    ManagedIceObject::ManagedIceObject(ManagedIceObject const& other)
        : IceUtil::Shared()
        , impl(new Impl)
    {
        impl->name = other.impl->name;
        impl->armarXManager = other.impl->armarXManager;
        impl->iceManager = nullptr;
        impl->objectScheduler = nullptr;
        impl->objectState = eManagedIceObjectCreated;
        impl->proxy = nullptr;
        impl->objectAdapter = nullptr;
        impl->connectivity = other.impl->connectivity;
        impl->profiler = other.impl->profiler;
        impl->enableProfilerFunction = &ManagedIceObject::Noop;
    }

    std::string ManagedIceObject::getName() const
    {
        if (impl->name.empty())
        {
            return getDefaultName();
        }

        return impl->name;
    }

    std::string ManagedIceObject::generateSubObjectName(const std::string& superObjectName, const std::string& subObjectName)
    {
        return superObjectName + "." + subObjectName;
    }

    std::string ManagedIceObject::generateSubObjectName(const std::string& subObjectName)
    {
        return generateSubObjectName(getName(), subObjectName);
    }


    ManagedIceObject::~ManagedIceObject()
    {
        for (const auto& [name, task] : impl->periodicTasks)
        {
            if (task)
            {
                task->stop();
            }
        }
        impl->periodicTasks.clear();
    }

    Ice::ObjectAdapterPtr ManagedIceObject::getObjectAdapter() const
    {
        return impl->objectAdapter;
    }



    // *******************************************************
    // establish dependencies
    // *******************************************************

    bool  ManagedIceObject::usingProxy(const std::string& name, const std::string&)
    {
        ARMARX_TRACE;
        if (name.empty())
        {
            throw LocalException("proxyName must not be empty.");
        }

        std::unique_lock lock(impl->connectivityMutex);

        // check if proxy already exists
        if (impl->connectivity.dependencies.find(name) != impl->connectivity.dependencies.end())
        {
            return false;
        }

        // add dependency
        ManagedIceObjectDependencyPtr proxyDependency = new ProxyDependency(getIceManager(), name);
        impl->connectivity.dependencies.insert(std::make_pair(name, proxyDependency));

        return true;
    }

    void ManagedIceObject::waitForProxy(const std::string& name, bool addToDependencies)
    {
        if (name.empty())
        {
            throw LocalException("The proxy name must not be empty.");
        }

        if (getState() < eManagedIceObjectStarting)
        {
            throw LocalException("Calling getProxy before component has been started");
        }

        if (addToDependencies)
        {
            usingProxy(name);

            waitForObjectScheduler();
        }
    }

    std::vector<std::string> ManagedIceObject::getUnresolvedDependencies() const
    {
        ARMARX_TRACE;
        ManagedIceObjectConnectivity con = getConnectivity();
        std::vector<std::string> dependencies;

        for (auto& dependencie : con.dependencies)
        {
            ManagedIceObjectDependencyBasePtr& dep = dependencie.second;

            if (!dep->getResolved())
            {
                dependencies.push_back(dep->getName());
            }
        }

        return dependencies;
    }

    std::string ManagedIceObject::GetObjectStateAsString(int state)
    {
        switch (state)
        {
            case eManagedIceObjectCreated:
                return "Created";

            case eManagedIceObjectInitializing:
                return "Initializing";

            case eManagedIceObjectInitialized:
                return "Initialized";
            case eManagedIceObjectInitializationFailed:
                return "InitializationFailed";

            case eManagedIceObjectStarting:
                return "Starting";

            case eManagedIceObjectStartingFailed:
                return "StartingFailed";

            case eManagedIceObjectStarted:
                return "Started";

            case eManagedIceObjectExiting:
                return "Exiting";

            case eManagedIceObjectExited:
                return "Exited";
        }

        return "Unknown";
    }


    void ManagedIceObject::usingTopic(const std::string& name, bool orderedPublishing)
    {
        ARMARX_TRACE;
        std::unique_lock lock(impl->connectivityMutex);

        // check if topic is already used
        if (std::find(impl->connectivity.usedTopics.begin(), impl->connectivity.usedTopics.end(), name) != impl->connectivity.usedTopics.end())
        {
            return;
        }

        // add dependency
        impl->connectivity.usedTopics.push_back(name);
        impl->orderedTopicPublishing[name] = orderedPublishing;
        ARMARX_VERBOSE << "Using topic with name: '" << name << "'";

        // only subscribe direcly if component has been initialized. Otherwise the dependency resolution will take care
        if (getState() >= eManagedIceObjectStarting)
        {
            getIceManager()->subscribeTopic(impl->proxy, name, orderedPublishing);
        }
    }

    bool ManagedIceObject::unsubscribeFromTopic(const std::string& name)
    {
        ARMARX_TRACE;
        std::unique_lock lock(impl->connectivityMutex);

        ARMARX_CHECK_EXPRESSION(impl->proxy);
        getIceManager()->unsubscribeTopic(impl->proxy, name);
        // check if topic is already used
        auto it = std::find(impl->connectivity.usedTopics.begin(), impl->connectivity.usedTopics.end(), name);
        if (it != impl->connectivity.usedTopics.end())
        {
            impl->connectivity.usedTopics.erase(it);
            ARMARX_VERBOSE << "Removing " << name << " from used topic list";
            return true;
        }
        return false;
    }


    void ManagedIceObject::offeringTopic(const std::string& name)
    {
        ARMARX_TRACE;
        std::unique_lock lock(impl->connectivityMutex);
        auto topicName = name + getIceManager()->getTopicSuffix();
        // check if topic is already used
        if (std::find(impl->connectivity.offeredTopics.begin(), impl->connectivity.offeredTopics.end(), topicName) != impl->connectivity.offeredTopics.end())
        {
            return;
        }

        // add dependency
        impl->connectivity.offeredTopics.push_back(topicName);
        ARMARX_INFO << "Offering topic with name: '" << topicName << "'";

        // only retrieve topic direcly if component has been initialized. Otherwise the dependency resolution will take care
        if (getState() >= eManagedIceObjectStarting)
        {
            getIceManager()->getTopic<IceStorm::TopicPrx>(name);
        }
    }

    void ManagedIceObject::preambleGetTopic(const std::string& name)
    {
        if (getState() < eManagedIceObjectStarting)
        {
            throw LocalException("Calling getTopic before component has been started");
        }

        // make sure proxy is on the dependency list
        offeringTopic(name);
    }




    bool ManagedIceObject::removeProxyDependency(const std::string& name)
    {
        ARMARX_TRACE;
        std::unique_lock lock(impl->connectivityMutex);

        // check if proxy already exists
        DependencyMap::iterator it = impl->connectivity.dependencies.find(name);

        if (it == impl->connectivity.dependencies.end())
        {
            return false;
        }

        ARMARX_VERBOSE << "Removing proxy '" << name << "' from dependency list.";
        impl->connectivity.dependencies.erase(it);
        getObjectScheduler()->wakeupDependencyCheck();
        return true;
    }

    // *******************************************************
    // getters
    // *******************************************************
    ArmarXManagerPtr ManagedIceObject::getArmarXManager() const
    {
        return impl->armarXManager;
    }

    IceManagerPtr ManagedIceObject::getIceManager() const
    {
        return impl->iceManager;
    }


    Profiler::ProfilerPtr ManagedIceObject::getProfiler() const
    {
        return impl->profiler;
    }

    void ManagedIceObject::enableProfiler(bool enable)
    {
        ARMARX_TRACE;
        Profiler::LoggingStrategyPtr strategy(new Profiler::LoggingStrategy);
        // set to empty log strategy if enable == false
        impl->enableProfilerFunction = &ManagedIceObject::Noop;

        if (!enable)
        {
            impl->profiler->setLoggingStrategy(strategy);
            return;
        }

        // enable the IceLoggingStrategy if IceManager is available
        // otherwise defer the creation until ManagedIceObject::init() is called
        if (getIceManager())
        {
            // create the IceLoggingStrategy immediately if Ice is available
            //enableProfilerFunction(this);
            ManagedIceObject::EnableProfilerOn(this);
        }
        else
        {
            // if Ice is not yet available, the creation of IceLoggingStrategy is delayed via the enableProfilerFunction
            impl->enableProfilerFunction = &ManagedIceObject::EnableProfilerOn;
        }
    }


    Ice::ObjectPrx ManagedIceObject::getProxy(long timeoutMs, bool waitForScheduler) const
    {
        ARMARX_TRACE;
        ArmarXObjectSchedulerPtr oSched = getObjectScheduler();
        IceUtil::Time startTime = TimeUtil::GetTime(true);
        if (waitForScheduler)
        {
            while (!oSched && ((TimeUtil::GetTime(true) - startTime).toMilliSecondsDouble() < timeoutMs || timeoutMs < 0))
            {
                oSched = getObjectScheduler();
                TimeUtil::USleep(10000);
            }
        }
        if (oSched)
        {
            oSched->waitForObjectStateMinimum(eManagedIceObjectStarting, timeoutMs);
        }
        else
        {
            ARMARX_WARNING_S << "called on an object without a object scheduler. To fix this add the object to an ArmarXManager prior to calling this function.";
        }
        return impl->proxy;
    }

    // *******************************************************
    // termination
    // *******************************************************
    void ManagedIceObject::terminate()
    {
        impl->stateCondition.notify_all();
        impl->objectScheduler->terminate();
    }

    void ManagedIceObject::setName(std::string name)
    {
        this->impl->name = name;
    }

    Ice::CommunicatorPtr ManagedIceObject::getCommunicator() const
    {
        return getIceManager()->getCommunicator();
    }

    // *******************************************************
    // phase handling
    // *******************************************************
    void ManagedIceObject::init(IceManagerPtr iceManager)
    {
        ARMARX_TRACE;
        // set state
        setObjectState(eManagedIceObjectInitializing);

        // set ice manager
        this->impl->iceManager = iceManager;

        // evaluate function created by enableProfiling();
        impl->enableProfilerFunction(this);

        // call framwork hook
        ARMARX_DEBUG << "call preOnInitComponent for all plugins...";
        foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ") preOnInitComponent...";
            plugin->preOnInitComponent();
            ARMARX_DEBUG << "plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ") preOnInitComponent...done!";
        }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
        ARMARX_DEBUG << "call preOnInitComponent for all plugins...done!";
        ARMARX_TRACE;

        ARMARX_DEBUG << "call preOnInitComponent...";
        preOnInitComponent();
        ARMARX_DEBUG << "call preOnInitComponent...done!";

        ARMARX_DEBUG << "call onInitComponent...";
        onInitComponent();
        ARMARX_DEBUG << "call onInitComponent...done!";

        ARMARX_DEBUG << "call postOnInitComponent...";
        postOnInitComponent();
        ARMARX_DEBUG << "call postOnInitComponent...done!";

        ARMARX_DEBUG << "call postOnInitComponent for all plugins...";
        foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ") postOnInitComponent...";
            plugin->postOnInitComponent();
            ARMARX_DEBUG << "plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ") postOnInitComponent...done!";
        }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
        ARMARX_DEBUG << "call postOnInitComponent for all plugins...done!";

        // set state
        setObjectState(eManagedIceObjectInitialized);
    }

    void ManagedIceObject::start(Ice::ObjectPrx& proxy, const Ice::ObjectAdapterPtr& objectAdapter)
    {
        ARMARX_TRACE;
        // set members
        this->impl->proxy = proxy;
        this->impl->objectAdapter = objectAdapter;

        // set state
        setObjectState(eManagedIceObjectStarting);

        // call framwork hook
        ARMARX_DEBUG << "call preOnConnectComponent for all plugins...";
        foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ") preOnConnectComponent...";
            plugin->preOnConnectComponent();
            ARMARX_DEBUG << "plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ") preOnConnectComponent...done!";
        }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
        ARMARX_DEBUG << "call preOnConnectComponent for all plugins...done!";
        ARMARX_TRACE;

        ARMARX_DEBUG << "call preOnConnectComponent...";
        preOnConnectComponent();
        ARMARX_DEBUG << "call preOnConnectComponent...done!";

        ARMARX_DEBUG << "call onConnectComponent...";
        onConnectComponent();
        ARMARX_DEBUG << "call onConnectComponent...done!";

        ARMARX_DEBUG << "call postOnConnectComponent...";
        postOnConnectComponent();
        ARMARX_DEBUG << "call postOnConnectComponent...done!";


        ARMARX_DEBUG << "call postOnConnectComponent for all plugins...";
        foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ") postOnConnectComponent...";
            plugin->postOnConnectComponent();
            ARMARX_DEBUG << "plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ") postOnConnectComponent...done!";
        }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
        ARMARX_DEBUG << "call postOnConnectComponent for all plugins...done!";

        // set state
        setObjectState(eManagedIceObjectStarted);
    }

    void ManagedIceObject::disconnect()
    {
        ARMARX_TRACE;
        // set state

        try
        {
            // call framwork hook
            ARMARX_DEBUG << "call preOnDisconnectComponent for all plugins...";
            foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
            {
                ARMARX_TRACE;
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") preOnDisconnectComponent...";
                plugin->preOnDisconnectComponent();
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") preOnDisconnectComponent...done!";
            }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
            ARMARX_DEBUG << "call preOnDisconnectComponent for all plugins...done!";
            ARMARX_TRACE;

            ARMARX_DEBUG << "call preOnDisconnectComponent...";
            preOnDisconnectComponent();
            ARMARX_DEBUG << "call preOnDisconnectComponent...done!";

            ARMARX_DEBUG << "call onDisconnectComponent...";
            onDisconnectComponent();
            ARMARX_DEBUG << "call onDisconnectComponent...done!";

            ARMARX_DEBUG << "call postOnDisconnectComponent...";
            postOnDisconnectComponent();
            ARMARX_DEBUG << "call postOnDisconnectComponent...done!";

            ARMARX_DEBUG << "call postOnDisconnectComponent for all plugins...";
            foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
            {
                ARMARX_TRACE;
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") postOnDisconnectComponent...";
                plugin->postOnDisconnectComponent();
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") postOnDisconnectComponent...done!";
            }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
            ARMARX_DEBUG << "call postOnDisconnectComponent for all plugins...done!";
            // set state
        }
        catch (...) // dispatch and handle exception
        {
            handleExceptions();
        }
        setObjectState(eManagedIceObjectInitialized);

    }

    void ManagedIceObject::exit()
    {
        ARMARX_TRACE;
        // set state
        setObjectState(eManagedIceObjectExiting);

        Ice::Identity id;
        id.name = getName();

        // remove self so no new calls are coming in
        try
        {
            ARMARX_TRACE;
            if (getObjectAdapter())
            {
                getObjectAdapter()->remove(id);
            }
        }
        catch (Ice::ObjectAdapterDeactivatedException&)
        {
        }
        catch (Ice::NotRegisteredException&)
        {
        }

        try
        {
            // call framwork hook
            ARMARX_DEBUG << "call preOnExitComponent for all plugins...";
            foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
            {
                ARMARX_TRACE;
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") preOnExitComponent...";
                plugin->preOnExitComponent();
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") preOnExitComponent...done!";
            }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
            ARMARX_DEBUG << "call preOnExitComponent for all plugins...done!";
            ARMARX_TRACE;

            ARMARX_DEBUG << "call preOnExitComponent...";
            preOnExitComponent();
            ARMARX_DEBUG << "call preOnExitComponent...done!";

            ARMARX_DEBUG << "call onExitComponent...";
            onExitComponent();
            ARMARX_DEBUG << "call onExitComponent...done!";

            ARMARX_DEBUG << "call postOnExitComponent...";
            postOnExitComponent();
            ARMARX_DEBUG << "call postOnExitComponent...done!";

            ARMARX_DEBUG << "call postOnExitComponent for all plugins...";
            foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
            {
                ARMARX_TRACE;
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") postOnExitComponent...";
                plugin->postOnExitComponent();
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") postOnExitComponent...done!";
            }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
            ARMARX_DEBUG << "call postOnExitComponent for all plugins...done!";
        }
        catch (...) // dispatch and handle exception
        {
            handleExceptions();
        }
        // set state
        setObjectState(eManagedIceObjectExited);
        impl->armarXManager = nullptr;
        impl->iceManager = nullptr;
        impl->objectScheduler = nullptr;
        impl->objectAdapter = nullptr;
        impl->proxy = nullptr;
        ARMARX_VERBOSE << "Object '" << this->getName() << "' is finished";
    }

    void ManagedIceObject::setObjectState(int newState)
    {
        ARMARX_TRACE;
        std::unique_lock lock(impl->objectStateMutex);
        ARMARX_DEBUG << "setObjectState: " << impl->objectState << " -> " << newState;

        if (newState == impl->objectState)
        {
            return;
        }

        impl->objectState = (ManagedIceObjectState)newState;
        impl->stateCondition.notify_all();
    }

    void ManagedIceObject::Noop(ManagedIceObject*)
    {
        // NOOP
    }

    void ManagedIceObject::EnableProfilerOn(ManagedIceObject* object)
    {
        ARMARX_TRACE;
        object->offeringTopic(armarx::Profiler::PROFILER_TOPIC_NAME);
        ProfilerListenerPrx profilerTopic = object->getIceManager()->getTopic<ProfilerListenerPrx>(armarx::Profiler::PROFILER_TOPIC_NAME);
        Profiler::LoggingStrategyPtr strategy(new Profiler::IceLoggingStrategy(profilerTopic));
        object->impl->profiler->setLoggingStrategy(strategy);
        ARMARX_INFO_S << "Profiler enabled for " << object->getName();
    }

    int ManagedIceObject::getState() const
    {
        std::unique_lock lock(impl->objectStateMutex);
        return impl->objectState;
    }

    ArmarXObjectSchedulerPtr ManagedIceObject::getObjectScheduler() const
    {
        return impl->objectScheduler;
    }


    ManagedIceObjectConnectivity ManagedIceObject::getConnectivity() const
    {
        std::unique_lock lock(impl->connectivityMutex);
        return impl->connectivity;
    }


    void armarx::ManagedIceObject::waitForObjectScheduler()
    {
        getObjectScheduler()->waitForDependencies();
    }

    void ManagedIceObject::setMetaInfo(const std::string& id, const VariantBasePtr& value)
    {
        std::unique_lock lock(impl->metaInfoMapMutex);
        impl->metaInfoMap[id] = value;
    }

    VariantBasePtr ManagedIceObject::getMetaInfo(const std::string& id)
    {
        std::unique_lock lock(impl->metaInfoMapMutex);
        return impl->metaInfoMap[id];
    }

    StringVariantBaseMap ManagedIceObject::getMetaInfoMap() const
    {
        std::unique_lock lock(impl->metaInfoMapMutex);
        return impl->metaInfoMap;
    }

    void ManagedIceObject::startPeriodicTask(const std::string& uniqueName,
            std::function<void(void)> f,
            int periodMs,
            bool assureMeanInterval,
            bool forceSystemTime)
    {
        ARMARX_TRACE;
        ARMARX_CHECK_EXPRESSION(!impl->periodicTasks.count(uniqueName))
                << "The name '" << uniqueName << "' is not unique!";
        impl->periodicTasks[uniqueName] = new SimplePeriodicTask(
            f, periodMs, assureMeanInterval, uniqueName, forceSystemTime);
    }
    bool ManagedIceObject::stopPeriodicTask(const std::string& name)
    {
        ARMARX_TRACE;
        if (!impl->periodicTasks.count(name))
        {
            return false;
        }
        const auto task = impl->periodicTasks.at(name);
        ARMARX_CHECK_NOT_NULL(task);
        task->stop();
        impl->periodicTasks.erase(name);
        return true;
    }
    ManagedIceObject::PeriodicTaskPtr ManagedIceObject::getPeriodicTask(const std::string& name)
    {
        ARMARX_TRACE;
        if (impl->periodicTasks.count(name))
        {
            return impl->periodicTasks.at(name);
        }
        return nullptr;
    }

    ManagedIceObjectPlugin::ManagedIceObjectPlugin(ManagedIceObject& parent, std::string pre) :
        _parent{&parent},
        _prefix{std::move(pre)}
    {}

    void armarx::ManagedIceObject::foreach_plugin(
        const std::function<void (std::type_index, const std::string&, ManagedIceObjectPlugin*)>& f,
        int line, const char* file, const char* function)
    {
        ARMARX_TRACE;
        ARMARX_DEBUG << "foreach_plugin called from "
                     << file << ':' << line
                     << " in function '" << function << "'";
        std::set<ManagedIceObjectPlugin*> processed;
        std::map<ManagedIceObjectPlugin*, std::tuple<std::type_index, std::string>> toProcess;
        for (const auto& [key, plugin] : _plugins)
        {
            const auto& [typeidx, name] = key;
            ARMARX_DEBUG << "adding plugin '" << name
                         << "' (" << GetTypeString(typeidx)
                         << ", @" << plugin.get() << ") to process list";

            toProcess.emplace(plugin.get(), key);
        }

        while (processed.size() < toProcess.size())
        {
            ARMARX_TRACE;
            const std::size_t oldNumProcessed = processed.size();
            ARMARX_DEBUG << "foreach_plugin: strill processing " << toProcess.size() - oldNumProcessed << " plugins";
            for (const auto& [plugin, key] : toProcess)
            {
                if (processed.count(plugin))
                {
                    continue;
                }
                const auto& [typeidx, name] = key;
                ARMARX_DEBUG << "processing plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ", @" << plugin << "): checking";
                bool blocked = false;
                for (const auto dep : plugin->_dependedOn)
                {
                    if (!processed.count(dep))
                    {
                        blocked = true;
                        break;
                    }
                }
                if (blocked)
                {
                    ARMARX_DEBUG << "processing plugin '" << name
                                 << "' (" << GetTypeString(typeidx)
                                 << ", @" << plugin << "): skipping for now";
                    continue;
                }

                ARMARX_DEBUG << "processing plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ", @" << plugin << ")': run functor";
                f(typeidx, name, plugin);
                processed.emplace(plugin);
            }
            if (processed.size() == oldNumProcessed)
            {
                std::stringstream str;
                str << "plugin graph is no DAG!\n";
                for (const auto& [plugin, key] : toProcess)
                {
                    const auto& [typeidx, name] = key;
                    str << "    plugin '" << name
                        << "' (" << GetTypeString(typeidx)
                        << ", @" << plugin << ")\n";
                    if (plugin->_dependedOn.empty())
                    {
                        str << "        has no dependencies\n";
                    }
                    else
                    {
                        for (const auto dep : plugin->_dependedOn)
                        {
                            str << "        depends on " << dep << '\n';
                        }
                    }
                }
                ARMARX_CHECK_EXPRESSION(false) << str.str();
            }
        }
    }

}
