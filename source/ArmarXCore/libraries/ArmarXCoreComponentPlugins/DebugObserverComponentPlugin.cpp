/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXGuiComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DebugObserverComponentPlugin.h"

namespace armarx::plugins
{
    void DebugObserverComponentPlugin::setDebugObserverTopic(const std::string& name)
    {
        ARMARX_CHECK_NOT_EMPTY(name);
        ARMARX_CHECK_EMPTY(_topicName);
        _topicName = name;
    }

    std::string DebugObserverComponentPlugin::getDebugObserverTopic()
    {
        return _topicName;
    }

    void DebugObserverComponentPlugin::preOnInitComponent()
    {
        if (_topicName.empty())
        {
            parent<Component>().getProperty(_topicName, makePropertyName(_propertyName));
        }
        parent<Component>().offeringTopic(_topicName);
    }

    void DebugObserverComponentPlugin::postOnInitComponent()
    {
        setChannelName(this);
    }

    void DebugObserverComponentPlugin::preOnConnectComponent()
    {
        if (!getDebugObserver())
        {
            setDebugObserver(parent<Component>()
                             .getTopic<DebugObserverInterfacePrx>(_topicName));

        }
    }

    void DebugObserverComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(_propertyName)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(_propertyName),
                "DebugObserver",
                "Name of the topic the DebugObserver listens on");
        }
    }
}

namespace armarx
{
    DebugObserverComponentPluginUser::DebugObserverComponentPluginUser()
    {
        addPlugin(_debugObserverComponentPlugin);
    }

    plugins::DebugObserverComponentPlugin& DebugObserverComponentPluginUser::getDebugObserverComponentPlugin()
    {
        return *_debugObserverComponentPlugin;
    }

    void DebugObserverComponentPluginUser::setDebugObserverChannel(
        const std::string& channelName,
        StringVariantBaseMap valueMap) const
    {
        _debugObserverComponentPlugin->setDebugObserverChannel(channelName, valueMap);
    }
    void DebugObserverComponentPluginUser::removeDebugObserverChannel(
        const std::string& channelname) const
    {
        _debugObserverComponentPlugin->removeDebugObserverChannel(channelname);
    }
    void DebugObserverComponentPluginUser::removeDebugObserverDatafield(
        const std::string& channelName,
        const std::string& datafieldName) const
    {
        _debugObserverComponentPlugin->removeDebugObserverDatafield(channelName, datafieldName);
    }

    void DebugObserverComponentPluginUser::removeAllDebugObserverChannels() const
    {
        _debugObserverComponentPlugin->removeAllDebugObserverChannels();
    }

    const DebugObserverInterfacePrx& DebugObserverComponentPluginUser::getDebugObserver() const
    {
        return _debugObserverComponentPlugin->getDebugObserver();
    }

    void DebugObserverComponentPluginUser::setDebugObserverBatchModeEnabled(bool enable)
    {
        _debugObserverComponentPlugin->setDebugObserverBatchModeEnabled(enable);
    }
    void DebugObserverComponentPluginUser::sendDebugObserverBatch()
    {
        _debugObserverComponentPlugin->sendDebugObserverBatch();
    }
}

