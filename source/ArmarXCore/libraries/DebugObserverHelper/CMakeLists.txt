set(LIB_NAME       DebugObserverHelper)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

armarx_add_library(
    LIBS     ArmarXCoreObservers
    SOURCES  DebugObserverHelper.cpp
    HEADERS  DebugObserverHelper.h
)


# add unit tests
add_subdirectory(test)
