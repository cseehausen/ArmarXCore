#pragma once

#include "IceManager.h"

#include <Ice/Identity.h>

#include <unordered_map>
#include <mutex>

namespace armarx
{

    struct IceManager::Impl
    {
        using ObjectRegistry = std::unordered_map<std::string, ObjectEntryPtr>  ;
        using ProxyMap = std::unordered_map<std::string, Ice::ObjectPrx>  ;

        /**
         * Application communicator
         */
        Ice::CommunicatorPtr communicator;

        /**
         * IceStorm Topic Manager proxy
         */
        IceStorm::TopicManagerPrx topicManagerProxy;

        /**
         * TopicManager instantiation mutex
         */
        std::mutex topicManagerMutex;

        std::mutex proxyCacheMutex;


        /**
         * Ice grid admin instance used administrate component objects
         * withing the IceGrid
         */
        IceGridAdminPtr iceGridAdmin;

        /**
         * IceGridAdmin instantiation mutex
         */
        std::mutex iceGridAdminMutex;

        /**
         * Session name
         */
        std::string name;

        /**
         * Object registry which contains object dependecy and IceStorm
         * topic information.
         */
        ObjectRegistry objectRegistry;

        /**
         * Mutex for adding, activating and removing objects
         */
        std::mutex objectRegistryMutex;

        /**
         * Retrieved and down casted checked proxies. If a requested proxy
         * resides in this map, a unchecked cast solely is performed before
         * returning the proxy.
         */
        ProxyMap checkedProxies;

        /**
          * Suffix for all topics. This is mainly used to direct all topics to another, unused topic
          * for replaying purposes.
          */
        std::string topicSuffix;
        /**
         * Retrieved topics map
         */
        std::map<std::string, IceStorm::TopicPrx> topics;

        /**
         * Subscribed IceStorm topics
         */
        std::vector<std::pair<std::string, Ice::ObjectPrx> > subscriptions;

        /**
         * Mutex for delayed topic retrieval and subscription registration
         */
        std::mutex topicRegistrationMutex;

        /**
         * Mutex for retrieving and subscribing topics
         */
        std::mutex topicRetrievalMutex;

        /**
         * Mutex for retrieving and subscribing topics
         */
        std::mutex topicSubscriptionMutex;

        /**
         * Flag of forcing shutdown procedure.
         */
        bool forceShutdown;
    };

    struct IceManager::ObjectEntry : public IceUtil::Shared
    {
        ObjectEntry() :
            name(std::string()),
            proxy(nullptr),
            adapter(nullptr),
            updated(true),
            dependenciesResolved(false),
            active(false),
            ownAdapter(true)
        {
        }

        std::string                 name;
        Ice::Identity               id;
        Ice::ObjectPrx              proxy;
        Ice::ObjectAdapterPtr       adapter;
        TopicList                   usedTopics;
        TopicList                   offeredTopics;
        DependencyList              dependencies;
        bool                        updated;
        bool                        dependenciesResolved;
        bool                        active;
        bool                        ownAdapter;
    };

}
