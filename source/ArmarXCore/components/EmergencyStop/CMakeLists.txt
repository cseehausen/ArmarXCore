armarx_component_set_name("EmergencyStop")

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore)

set(SOURCES EmergencyStop.cpp)
set(HEADERS EmergencyStop.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

armarx_generate_and_add_component_executable(
    COMPONENT_CLASS_NAME EmergencyStopMaster
)
