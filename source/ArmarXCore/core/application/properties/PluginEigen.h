/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <SimoxUtility/meta/eigen/is_eigen_array.h>
#include <SimoxUtility/meta/eigen/is_eigen_matrix.h>
#include <SimoxUtility/json.h>

#include <ArmarXCore/core/application/properties/PropertyDefinition.h>

//DefaultAsStringPlugin
namespace armarx::meta::properties
{
    template<class T>
    struct DefaultAsStringPlugin <
        T,
        std::enable_if_t <simox::meta::is_eigen_array_v<T>>
                > : std::true_type
    {
        static std::string Str(PropertyDefinition<T>& pd)
        {
            nlohmann::json j = pd.getDefaultValue();
            return j.dump();
        }
    };

    template<class T>
    struct DefaultAsStringPlugin <
        T,
        std::enable_if_t <simox::meta::is_eigen_matrix_v<T>>
                > : std::true_type
    {
        static std::string Str(PropertyDefinition<T>& pd)
        {
            using traits = simox::meta::is_eigen_matrix<T>;
            using eigen_t = typename traits::eigen_t;
            using scalar_t = typename traits::scalar_t;
            nlohmann::json j;

            // If row or column vector, make a coefficient vector and dump this as JSON.
            if constexpr(traits::cols == 1 or traits::rows == 1)
            {
                eigen_t m = pd.getDefaultValue();
                std::vector<scalar_t> coefs;
                for (unsigned int r = 0; r < traits::rows; ++r)
                {
                    for (unsigned int c = 0; c < traits::cols; ++c)
                    {
                        coefs.push_back(m(r, c));
                    }
                }
                j = coefs;
            }
            // Else to_json Eigen overload.
            else
            {
                j = pd.getDefaultValue();
            }
            return j.dump();
        }
    };
}
//PDInitHookPlugin
namespace armarx::meta::properties
{
    template<class T>
    struct PDInitHookPlugin <
        T,
        std::enable_if_t <simox::meta::is_eigen_array_v<T>>
                > : std::true_type
    {
        static void Init(PropertyDefinition<T>& pd)
        {
            using traits = simox::meta::is_eigen_array<T>;
            using eigen_t = typename traits::eigen_t;

            const static auto parser = [&](const std::string & input) -> eigen_t
            {
                return nlohmann::json::parse(input);
            };

            pd.setFactory(parser);
        }
    };

    template<class T>
    struct PDInitHookPlugin <
        T,
        std::enable_if_t <simox::meta::is_eigen_matrix_v<T>>
                > : std::true_type
    {
        static void Init(PropertyDefinition<T>& pd)
        {
            using traits = simox::meta::is_eigen_matrix<T>;
            using eigen_t = typename traits::eigen_t;
            using scalar_t = typename traits::scalar_t;

            const static auto parser = [&](const std::string & input) -> eigen_t
            {
                // Use from_json Eigen overload (no row/col vector).
                if constexpr(!(traits::cols == 1 or traits::rows == 1))
                {
                    return nlohmann::json::parse(input);
                }

                // If row or column vector, init Eigen vector from coefficient vector.
                eigen_t m = eigen_t::Zero();
                const std::vector<scalar_t> coefs = nlohmann::json::parse(input);

                if (static_cast<long int>(coefs.size()) != m.size())
                {
                    using armarx::exceptions::local::InvalidPropertyValueException;
                    throw InvalidPropertyValueException("unknown", input)
                            << "Property must be a vector with " << m.size() << " elements.";
                }

                unsigned int r = 0;
                unsigned int c = 0;
                for (unsigned int i = 0; i < coefs.size(); ++i)
                {
                    m(r, c) = coefs.at(i);
                    // Only increment r if column vector, or c if row vector.
                    if constexpr(traits::cols == 1)
                    {
                        ++r;
                    }
                    else
                    {
                        ++c;
                    }
                }
                return m;
            };

            pd.setFactory(parser);
        }
    };
}
