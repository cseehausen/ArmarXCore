armarx_component_set_name("@COMPONENT_NAME@")

set(COMPONENT_LIBS ArmarXCore)
set(SOURCES @COMPONENT_NAME@.cpp)
set(HEADERS @COMPONENT_NAME@.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")
# all target_include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
#if(MyLib_FOUND)
#    target_include_directories(@COMPONENT_NAME@ PUBLIC ${MyLib_INCLUDE_DIRS})
#endif()

# add unit tests
add_subdirectory(test)
