/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "../data_structure/ApplicationInstance.h"
#include <memory>

namespace ScenarioManager
{
    class StatusManager;
}

namespace ScenarioManager::Exec
{
    /**
    * @class ApplicationInstance
    * @ingroup Exec
    * @brief Interface for classes that handle the starting of applications
    * Classes implementing this interface also need to implement a method to get the status of an application
    */
    class ApplicationStarter
    {
    public:
        /**
        * Starts an application.
        * @param application application to be started.
        */
        virtual void startApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) = 0;
        virtual void startScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) = 0;

        virtual void deployApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) = 0;
        virtual void deployScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) = 0;


        /**
        * Returns the status of an application.
        * @param application application whose status is returned
        * @return status of the application
        */
        virtual std::string getStatus(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager) = 0;

        virtual bool isApplicationDeployed(Data_Structure::ApplicationInstancePtr application) = 0;
        virtual bool isScenarioDeployed(Data_Structure::ScenarioPtr scenario) = 0;

        std::string commandLineParameters;
    };

    using ApplicationStarterPtr = std::shared_ptr<ApplicationStarter>;
}
