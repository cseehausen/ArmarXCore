/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2019, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */


#include <ArmarXCore/core/time/ScopedStopWatch.h>


// STD/STL
#include <functional>

// Ice
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>


namespace armarx
{

    ScopedStopWatch::ScopedStopWatch(std::function<void(IceUtil::Time)> callback, TimeMode timeMode) :
        StopWatch(timeMode),
        m_callback{callback}
    {
        // pass
    }


    ScopedStopWatch::~ScopedStopWatch()
    {
        m_callback(stop());
    }
}
