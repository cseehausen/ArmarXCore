/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "LinuxStarter.h"
#include "../parser/iceparser.h"
#include "../parser/DependenciesGenerator.h"
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/util/OnScopeExit.h>

#include <SimoxUtility/algorithm/string/string_tools.h>
#include <boost/process.hpp>

#include <filesystem>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <signal.h>
#include <syslog.h>
#include <fcntl.h>
#include <future>
#include <thread>
#include <string>
#include <iostream>
#include <sstream>
#include <iostream>
#include <sys/time.h>

using namespace ScenarioManager;
using namespace Exec;
using namespace Parser;
//using namespace Data_Structure;
using namespace armarx;

std::string get_process_name_by_pid(const int pid)
{
    char* name = (char*)calloc(1024, sizeof(char));
    if (name)
    {
        sprintf(name, "/proc/%d/cmdline", pid);
        FILE* f = fopen(name, "r");
        if (f)
        {
            size_t size;
            size = fread(name, sizeof(char), 1024, f);
            if (size > 0)
            {
                if ('\n' == name[size - 1])
                {
                    name[size - 1] = '\0';
                }
            }
            fclose(f);
        }
        std::string result(name);
        free(name);
        return result;
    }
    return std::string();
}

void LinuxStarter::deployApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    application->setStatusWriteBlock(false);
}

void LinuxStarter::deployScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    scenario->setStatusWriteBlock(false);
}


void LinuxStarter::startScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    ARMARX_ON_SCOPE_EXIT
    {
        scenario->setStatusWriteBlock(false);
    };
    std::vector<std::future<void>> futures;

    std::vector<ApplicationInstancePtr> apps = *scenario->getApplications();
    for (auto it = apps.begin(); it != apps.end(); it++)
    {
        futures.push_back(startApplicationAsync(*it, statusManager, commandLineParameters, printOnly));
    }

    for (auto future = futures.begin(); future != futures.end(); ++future)
    {
        future->wait();
    }
}


void LinuxStarter::startApplication(ApplicationInstancePtr app, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    ARMARX_ON_SCOPE_EXIT
    {
        app->setStatusWriteBlock(false);
    };
    if (!app->getEnabled())
    {
        return;
    }

    std::string runName = app->getName();
    if (app->getName().find("Run") == std::string::npos)
    {
        runName.append("Run");
    }

    std::filesystem::path componentFolderPath = std::filesystem::path(ArmarXDataPath::GetCachePath()) / "ComponentFiles";

    if (!std::filesystem::exists(componentFolderPath))
    {
        if (!std::filesystem::create_directories(componentFolderPath))
        {
            std::cout << "Could not create Cache folder for ScenarioManagerPlugin at " << componentFolderPath.string() << std::endl;
        }
    }


    std::filesystem::path dependenciesFilePath = componentFolderPath / std::filesystem::path("./" + app->getScenario()->getPackage()->getName() + ".dependencies.cfg");

    if (!std::filesystem::exists(dependenciesFilePath))
    {
        Parser::DependenciesGenerator builder;
        builder.generateDependenciesCfg(Data_Structure::PackagePtr(app->getScenario()->getPackage()));
    }

    if (!std::filesystem::exists(std::filesystem::path(app->getScenario()->getGlobalConfigPath()))
        || !std::filesystem::exists(std::filesystem::path(app->getConfigPath()))
        || !std::filesystem::exists(dependenciesFilePath))
    {
        std::cout << "Launching " << app->getName() << " in " << app->getScenario()->getPackage()->getName() << " without an needed cfg file";
    }
    if (!std::filesystem::exists(std::filesystem::path(app->getPathToExecutable().append("/").append(runName))))
    {
        std::cout << "\033[1;31m" << "Warning: Could not launch " << app->getName() << " in " << app->getScenario()->getPackage()->getName() << " because the executable is missing at " << app->getPathToExecutable() << "\033[0m" << std::endl;
    }
    std::string args;
    args += app->getPathToExecutable().append("/").append(runName);
    args += std::string(" --Ice.Config=").append(app->getScenario()->getGlobalConfigPath())
            .append(",").append(app->getConfigPath());
    auto configDomain = app->getConfigDomain();
    ARMARX_CHECK_EXPRESSION(!configDomain.empty());
    args += std::string(" --" + configDomain + ".DependenciesConfig=").append(dependenciesFilePath.string());
    args += std::string(" --" + configDomain + ".LoggingGroup=").append(app->getScenario()->getName());

    if (!commandLineParameters.empty())
    {
        Ice::StringSeq additional = Split(commandLineParameters, " ");

        bool inString = false;
        std::string inStringChar;
        std::string toPush = "";
        for (auto command : additional)
        {
            if (!inString && command.find("\"") != std::string::npos)
            {
                inString = true;
                inStringChar = "\"";
            }
            else if (!inString && command.find("\'") != std::string::npos)
            {
                inString = true;
                inStringChar = "\'";
            }
            else if (inString && command.find(inStringChar) != std::string::npos)
            {
                inString = false;
                toPush += command;
                args += " " + toPush;
                toPush = "";
                continue;
            }

            if (inString)
            {
                toPush += command + " ";
            }
            else
            {
                if (command != "")
                {
                    args += " " + command;
                }
            }
        }
    }
    else
    {
        Ice::StringSeq additional = Split(this->commandLineParameters, " ");
        for (auto arg : additional)
        {
            args += arg + " ";
        }
    }

    using namespace boost::process;
    using namespace boost::process::initializers;
    std::string appPath = app->getPathToExecutable().append("/").append(runName);
#if defined(BOOST_POSIX_API)
    // prevent child to become a zombie process because the parent has reacted on a signal yet
    signal(SIGCHLD, SIG_IGN);
#endif

    if (printOnly)
    {
        for (auto arg : args)
        {
            std::cout << arg;
        }
        std::cout << " &" << std::endl << std::endl;
        return;
    }

    //this apperantly prodcuces escaping errors
    //child c = execute(set_args(args), inherit_env());

    //this is the solution
    child c = execute(run_exe(appPath), set_cmd_line(args), inherit_env());

    int pid = c.pid;

    app->setPid(pid);
    statusManager.savePid(app);

}

std::string LinuxStarter::getStatus(ApplicationInstancePtr application, StatusManager statusManager)
{
    //if the app has no pid ask the pid manager if there is an cached Pid for this app
    if (application->getPid() < 0)
    {
        application->setPid(statusManager.loadPid(application));

        //if there is none just return stopped
        if (application->getPid() < 0)
        {
            return Data_Structure::ApplicationStatus::Stopped;
        }
        else
        {
            std::string systemAppName = getSystemAppName(application);
            //std::string systemAppName = get(application->getPid());


            //if there is no more file there the app got stopped while checking (should only occure very rarely)
            if (systemAppName.empty())
            {
                application->setPid(-1);
                statusManager.savePid(application);
                return Data_Structure::ApplicationStatus::Stopped;
            }
            else
            {
                std::string runName = application->getName().append("Run");
                if (systemAppName.compare(runName) == 0)
                {
                    return getStatus(application, statusManager);
                }
                else
                {
                    //else delete the saved pid and inform the user
                    ARMARX_INFO_S << "The Process name with Pid (" << application->getPid() << ":" << systemAppName << ") does not correspond with app name (" << application->getName() << "). Resetting Saved Pid.";
                    application->setPid(-1);
                    statusManager.savePid(application);
                    return Data_Structure::ApplicationStatus::Stopped;
                }
            }
        }
    }
    else
    {
        std::string status = getSystemAppStatus(application);
        if (status == Data_Structure::ApplicationStatus::Stopped)
        {
            application->setPid(-1);
            statusManager.savePid(application);
            return Data_Structure::ApplicationStatus::Stopped;
        }
        else
        {
            return status;
        }
    }
}

std::string LinuxStarter::getSystemAppName(Data_Structure::ApplicationInstancePtr application)
{
    std::string namepluspath = get_process_name_by_pid(application->getPid());

    if (namepluspath.empty())
    {
        return "";
    }
    else
    {
        std::string result = namepluspath.substr(namepluspath.rfind("/") + 1);

        return result;
    }
}

std::string LinuxStarter::getSystemAppStatus(Data_Structure::ApplicationInstancePtr application)
{
    std::string processFilePath = "/proc/";
    processFilePath << application->getPid() << "/status";
    std::ifstream t(processFilePath);

    if (!t.is_open())
    {
        return Data_Structure::ApplicationStatus::Stopped;
    }

    std::string stateLine;
    std::string line;
    while (std::getline(t, line))
    {
        if (Contains(line, "State:"))
        {
            stateLine = line;
            break;
        }
    }
    if (stateLine.empty())
    {
        return Data_Structure::ApplicationStatus::Stopped;
    }
    std::string result = stateLine.substr(stateLine.find(':') + 1);

    simox::alg::trim(result);

    result = result.at(0);

    if (result == "R" || result == "S")
    {
        return Data_Structure::ApplicationStatus::Running;
    }
    else if (result == "D" || result == "Z")
    {
        return Data_Structure::ApplicationStatus::Unknown;
    }
    else if (result == "T" || result == "X")
    {
        return Data_Structure::ApplicationStatus::Stopped;
    }
    else
    {
        ARMARX_DEBUG_S << "Uncatched app return state";
        return Data_Structure::ApplicationStatus::Unknown;
    }
}

std::future<void> LinuxStarter::startApplicationAsync(ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    if (application->getPid() != -1)
    {
        application->setStatusWriteBlock(false);
        return std::future<void>();
    }

    std::packaged_task<void()> task(std::bind(&ApplicationStarter::startApplication, this, application, statusManager, commandLineParameters, printOnly)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread t(std::move(task));
    if (printOnly)
    {
        t.join(); //if only the commands should be printed then we want sync behaviour
    }
    else
    {
        t.detach();
    }
    return result;
}

bool LinuxStarter::isApplicationDeployed(Data_Structure::ApplicationInstancePtr application)
{
    return false;
}

bool LinuxStarter::isScenarioDeployed(Data_Structure::ScenarioPtr scenario)
{
    return false;
}
