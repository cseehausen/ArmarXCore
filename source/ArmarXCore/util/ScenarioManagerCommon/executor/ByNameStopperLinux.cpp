/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ByNameStopperLinux.h"
#include <ArmarXCore/core/exceptions/Exception.h>

using namespace ScenarioManager;

void Exec::ByNameStopperLinux::stop(Data_Structure::ApplicationInstancePtr application)
{
    killAllShellCall("-15", application->getName());
}

void Exec::ByNameStopperLinux::kill(Data_Structure::ApplicationInstancePtr application)
{
    killAllShellCall("-9", application->getName());
}

void Exec::ByNameStopperLinux::killAllShellCall(std::string sig, std::string applicationName)
{
    std::string runName = applicationName;
    if (applicationName.find("Run") == std::string::npos)
    {
        runName.append("Run");
    }
    std::string strCommand = "killall " + sig + " \"" + runName + "\"";
    const char* cCommand = strCommand.c_str();
    int status = system(cCommand);

    if (status == -1)
    {
        throw armarx::LocalException("Unsuccessfull shell command call. Tried to call \" " + strCommand + " \".");
    }
}

void Exec::ByNameStopperLinux::removeApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager)
{
    throw "ScenarioManager error: using false application stopper for this Scenario, Should not happen!";
}

void Exec::ByNameStopperLinux::removeScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager)
{
    throw "ScenarioManager error: using false application stopper for this Scenario, Should not happen!";
}
