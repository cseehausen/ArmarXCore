#pragma once

#include <exception>
#include <sstream>
#include <string>

#include <ArmarXCore/core/util/StringHelpers.h>
#include <ArmarXCore/util/CPPUtility/TemplateMetaProgramming.h>

namespace armarx
{
    /**
     * \class LocalException
     * \ingroup Exceptions
     * \brief Standard exception type for ArmarX, that should not be transmitted via Ice.
     *
     *
     * Offers a backtrace to where the exception was thrown.<br/>
     * All local exceptions must inherit from this class.
     */
    class LocalException: public std::exception
    {
        mutable std::string resultingMessage;
    public:
        /**
         * @brief LocalException Constructor
         */
        LocalException();
        /**
         * @brief LocalException Copy Constructor
         */
        LocalException(const LocalException& e);
        /**
         * @brief LocalException Constructor taking a \p reason string as parameter
         */
        LocalException(const std::string& reason)
        {
            backtrace = generateBacktrace();
            setReason(reason);
        }

        /**
         * @brief LocalException Destructor
         */
        ~LocalException() noexcept override { }

        /**
         * @return string containing the reason of the exception
         */
        const char* what() const noexcept override;
        /**
         * @return the exception name
         */
        virtual std::string name() const
        {
            return "armarx::LocalException";
        }

        /**
         * @param reason string containing an explanation about the reason why the exception occured
         */
        void setReason(const std::string& reason);


        /**
         * @return string containing the reason without backtrace
         */
        std::string getReason() const;

        /**
         * @return string containing the backtrace
         */
        static std::string generateBacktrace();

        template<typename T>
        LocalException& operator<<(const T& message)
        {
            // the operator <<(ostream, vector) is defined in two headers
            // the implementations behave differently.
            // here we explicitly selet the operator we want
            if constexpr(meta::TypeTemplateTraits::IsInstanceOfV<std::vector, T>)
            {
                std::operator <<(reason, message);
            }
            else
            {
                reason << message;
            }
            return *this;
        }
    private:
        /**
         * Generates an explanatory string by concatenating all LocalException
         * information and stores it in armarx::LocalException::resultingMessage.
         */
        void generateOutputString() const;

        /**
         * @brief reason is a string holding an explanation of why the exception was thrown
         */
        std::stringstream reason;
        /**
         * @brief backtrace is a string containing the backtrace from where the exception was thrown
         */
        std::string backtrace;
    };


    /**
     * \ingroup Exceptions
     * Print the output of common armarx exceptions.
     *
     * Handles exception types like armarx::LocalException, armarx::UserException,
     * IceUtil::Exception and std::exception.
     *
     * If available, the backtrace is printed out.
     *
     * example:
     * \code
     * try{
     *   //...your code
     * }
     * catch(...)
     * {
     *   handleExceptions();
     *   // your special exception handling
     * }
     * \endcode
     */
    extern "C" void handleExceptions();

    /**
     * \ingroup Exceptions
     * Generates a string from an exception with additional information for some exceptions, like Ice::NoObjectFactoryException.

     * example:
     * \code
     * try{
     *   //...your code
     * }
     * catch(...)
     * {
     *   ARMARX_ERROR_S << GetHandledExceptionString();
     *   // your special exception handling
     * }
     */
    std::string GetHandledExceptionString();
}
