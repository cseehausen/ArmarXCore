# Macro to check ArmarX generated files

add_custom_target(CHECK_GENERATED_FILES)

include(CMakeParseArguments)

macro(armarx_check_generated_files)

    message(STATUS "\n== Check generated files and remove them if necessary ... ==")

    get_property(CHECKED_GENERATED_FILES GLOBAL PROPERTY checked_generated_files)
    if(NOT CHECKED_GENERATED_FILES)
        set_property(GLOBAL PROPERTY checked_generated_files TRUE)
        #message("${ArmarXCore_BINARY_DIR}/armarx-dev" "checkGeneratedFiles" "${PROJECT_NAME}")
        execute_process(COMMAND "${ArmarXCore_BINARY_DIR}/armarx-dev" "checkGeneratedFiles" "${PROJECT_NAME}"
            RESULT_VARIABLE CMD_RESULT OUTPUT_VARIABLE CMD_OUTPUT)
        #message(STATUS ${CMD_RESULT})
        #message(STATUS ${CMD_OUTPUT})
        string(REGEX REPLACE "\n$" "" CMD_OUTPUT "${CMD_OUTPUT}")
        string(REPLACE "\n" ";" CMD_OUTPUT_LIST "${CMD_OUTPUT}")
        foreach(CMD_OUTPUT_LINE ${CMD_OUTPUT_LIST})
            message(STATUS ${CMD_OUTPUT_LINE})
        endforeach()
    endif()

    message(STATUS "Checking done")

endmacro()
