/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXCoreComponentPlugins
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <SimoxUtility/json/json.hpp>

namespace armarx
{
    struct JsonStorageRetrievedValue
    {
        std::string key;
        nlohmann::json value;
        std::string provider;
        long revision = 0;
        IceUtil::Time storeTimestamp;
        bool valid = false;
    };

    namespace plugins
    {

        /**
             * @class JsonStorageComponentPlugin
             * @ingroup Library-ArmarXCoreComponentPlugins
             * @brief Brief description of class JsonStorageComponentPlugin.
             *
             * Detailed description of class JsonStorageComponentPlugin.
             */
        struct JsonStorageComponentPlugin : ComponentPlugin
        {
            using ComponentPlugin::ComponentPlugin;

            ~JsonStorageComponentPlugin();

            void preOnInitComponent() override;
            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            void storeValue(std::string const& key, nlohmann::json const& value);
            JsonStorageRetrievedValue retrieveValue(std::string const& key);


            struct Impl;
            std::unique_ptr<Impl> impl;
        };
    }

    struct JsonStorageComponentPluginUser : virtual ManagedIceObject
    {
        using JsonStorageComponentPlugin = armarx::plugins::JsonStorageComponentPlugin;

        JsonStorageComponentPluginUser();

        void JsonStorage_storeValue(std::string const& key, nlohmann::json const& value);
        JsonStorageRetrievedValue JsonStorage_retrieveValue(std::string const& key);

        JsonStorageComponentPlugin* plugin{nullptr};
    };
}
