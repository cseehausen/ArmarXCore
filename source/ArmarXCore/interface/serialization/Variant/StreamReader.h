#pragma once

#include <Ice/StreamHelpers.h>

#include <ArmarXCore/util/CPPUtility/variant.h>

namespace Ice
{
    template<class S>
    struct StreamReader<boost::blank, S>
    {
        static void read(S*, boost::blank&) {}
    };

    template<class S, class...Ts>
    struct StreamReader<boost::variant<Ts...>, S>
    {
    private:
        template<int I, class...Params>
        struct Reader
        {
            static void read(S*, boost::variant<Ts...>&, int)
            {
                throw std::logic_error{"should not be possible to reach this"};
            }
        };
        template<int I, class T0, class...Tail>
        struct Reader<I, T0, Tail...>
        {
            static void read(S* str, boost::variant<Ts...>& v, int i)
            {
                if (I == i)
                {
                    T0 t0;
                    str->read(t0);
                    v = t0;
                }
                else
                {
                    Reader < I + 1, Tail... >::read(str, v, i);
                }
            }
        };
    public:
        static void read(S* str, boost::variant<Ts...>& v)
        {
            int i;
            str->read(i);
            Reader<0, Ts...>::read(str, v, i);
        }
    };

    //    template<class S>
    //    struct StreamReader<boost::variant<>, S>
    //    {
    //        static void read(S* str, boost::variant<>& v)
    //        {
    //        }
    //    };
}
