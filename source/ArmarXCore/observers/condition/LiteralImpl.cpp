/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/condition/LiteralImpl.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <Ice/Ice.h>

#include <cstdarg>
#include <mutex>

template class ::IceInternal::Handle<::armarx::LiteralImpl>;

namespace armarx
{
    struct LiteralImpl::Impl
    {
        CheckIdentifier checkIdentifier;
        Ice::ObjectPrx  myProxy;
        bool installed;
        std::mutex accessLock;
    };

    // list of parameters version
    LiteralImpl::LiteralImpl()
        : impl(new Impl)
    {
        this->type = eLiteral;
        impl->installed = false;
    }

    LiteralImpl::~LiteralImpl()
    {

    }

    LiteralImpl::LiteralImpl(const std::string& dataFieldIdentifierStr, const std::string& checkName, const ParameterList& checkParameters)
        : LiteralImpl()
    {
        init(dataFieldIdentifierStr, checkName, checkParameters);
    }

    LiteralImpl::LiteralImpl(const DataFieldIdentifier& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters)
        : LiteralImpl()
    {
        init(dataFieldIdentifier.getIdentifierStr(), checkName, checkParameters);
    }

    LiteralImpl::LiteralImpl(const DataFieldIdentifierPtr& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters)
        : LiteralImpl()
    {
        init(dataFieldIdentifier->getIdentifierStr(), checkName, checkParameters);
    }

    LiteralImpl::LiteralImpl(const DatafieldRefBasePtr& dataFieldIdentifier, const std::string& checkName, const ParameterList& checkParameters)
        : LiteralImpl()
    {
        DatafieldRefPtr ref = DatafieldRefPtr::dynamicCast(dataFieldIdentifier);
        init(ref->getDataFieldIdentifier()->getIdentifierStr(), checkName, checkParameters);
    }

    void LiteralImpl::createInstance()
    {

    }

    CheckConfiguration LiteralImpl::getCheckConfiguration(const Ice::Current& c)
    {
        return checkConfig;
    }

    void LiteralImpl::setValue(bool value, const Ice::Current& c)
    {
        this->value = value;
        update();
    }

    void LiteralImpl::setValueAndData(bool value, const DataFieldIdentifierBasePtr& id, const VariantBasePtr& data, const Ice::Current& c)
    {
        this->value = value;
        DataFieldIdentifierPtr dataId = DataFieldIdentifierPtr::dynamicCast(id);
        datafieldValues = StringVariantBaseMap {{dataId->getIdentifierStr(), data}};
        updateWithData();
    }

    Ice::ObjectPtr LiteralImpl::ice_clone() const
    {
        LiteralImplPtr literal = new LiteralImpl();
        literal->type = this->type;
        literal->impl->installed = this->impl->installed;
        literal->impl->checkIdentifier = this->impl->checkIdentifier;
        literal->checkConfig = this->checkConfig;
        literal->impl->myProxy = this->impl->myProxy;

        return literal;
    }

    void LiteralImpl::output(std::ostream& out) const
    {
        DataFieldIdentifierPtr dataFieldIdentifier = DataFieldIdentifierPtr::dynamicCast(checkConfig.dataFieldIdentifier);

        out << checkConfig.checkName << "(";

        if (dataFieldIdentifier)
        {
            out << dataFieldIdentifier;
        }
        else
        {
            out << "NULL";
        }


        ParameterList::const_iterator iter = checkConfig.checkParameters.begin();

        while (iter != checkConfig.checkParameters.end())
        {
            if (iter == checkConfig.checkParameters.begin())
            {
                out << ", ";
            }

            VariantPtr var = VariantPtr::dynamicCast(*iter);

            if (var)
            {
                out << var;
            }
            else
            {
                out << "NULL";
            }

            iter++;

            if (iter != checkConfig.checkParameters.end())
            {
                out << ",";
            }
        }

        out << ")";
    }

    void LiteralImpl::init(const std::string& dataFieldIdentifierStr, const std::string& checkName, const ParameterList& checkParameters)
    {
        type = eLiteral;

        DataFieldIdentifierPtr dataFieldIdentifier = new DataFieldIdentifier(dataFieldIdentifierStr);
        this->checkConfig.dataFieldIdentifier = dataFieldIdentifier;
        this->checkConfig.checkName = checkName;

        ParameterList::const_iterator iter = checkParameters.begin();

        while (iter != checkParameters.end())
        {
            this->checkConfig.checkParameters.push_back(VariantPtr(new Variant(*VariantPtr::dynamicCast(*iter))));
            iter++;
        }
    }

    void LiteralImpl::installCheck(const Ice::ObjectAdapterPtr& adapter, const ObserverInterfacePrx& proxy)
    {
        std::unique_lock lock(impl->accessLock);

        if (impl->installed)
        {
            return;
        }

        // register object
        impl->myProxy = adapter->addWithUUID(this);
        checkConfig.listener = LiteralImplBasePrx::uncheckedCast(impl->myProxy);

        // add object to ice
        impl->checkIdentifier = proxy->installCheck(checkConfig);

        impl->installed = true;
    }

    void LiteralImpl::removeCheck(const Ice::ObjectAdapterPtr& adapter, const ObserverInterfacePrx& proxy)
    {
        std::unique_lock lock(impl->accessLock);

        if (!impl->installed)
        {
            return;
        }

        // remove check
        proxy->removeCheck(impl->checkIdentifier);

        // unregister object
        adapter->remove(impl->myProxy->ice_getIdentity());

        impl->installed = false;
    }
}
