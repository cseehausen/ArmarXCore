/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ScenarioCliOptions.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <SimoxUtility/algorithm/string/string_tools.h>
#include <filesystem>
#include <iostream>

namespace armarx
{
    CmdOptions ScenarioManagerCliOptions::parseCmdOptions(int argc, char* argv[])
    {
        using namespace boost::program_options;
        CmdOptions options;
        options.error = false;
        options.showHelp = false;
        options.print = false;
        options.wait = false;
        options.scxPath = "";
        options.parameters = "";
        options.remote = false;

        // create program options
        options.description.reset(new options_description(std::string(
                                      "This executable is an application within the ArmarX framework.\n")
                                  + "==================================================================\n"
                                  + " Starting an ArmarX Scenario\n"
                                  + "==================================================================\n"
                                  + "In order to start a Scenario pass either a path to an scx file or a Scenario name with a command."
                                  + " Example command:\n\n"

                                  + "  > armarx scenario command scxpath/scxname [package name] [-a application]\n\n"

                                  + "Help options"));


        options.description->add_options()("help,h", "Help option");

        options.description->add_options()("command,c", value <std::string>(), "What should be done: {\n \tstart, \n \tdeploy, \n \tstop, \n \tkill, \n \tremove, \n \trestart (kills by default), \n \tlist (List all Scenario in Packages in ArmarX.DefaultPackages)\n \tstatus (Show the status of the Scenario)\n \t generate (generate the old local/remote icegrid xml configs)\n}");
        options.description->add_options()("file,f", value <std::string>()->default_value("."), "Path to the scx file (default: looks in current dir)");
        options.description->add_options()("package,p", value<std::string>(), "[optional] If the packages are included in your default config as ArmarX.DefaultPackages this can be left empty");
        options.description->add_options()("application,a", value<std::string>(), "[optional] Application out of the Scenario to execute the command with");
        options.description->add_options()("parameters", value<std::string>()->default_value(""), "[optional] Use this if you want to start an scenario with additional commandline parameters");
        options.description->add_options()("print", "Print the execution commands of the CLI instead of executing them");
        options.description->add_options()("wait,w", "Wait for all applications to quit before exiting");
        options.description->add_options()("remote", "[optional] if you want to deploy an Scenario remotely this has to be added to the command");

        positional_options_description pd;
        pd.add("command", 1);
        pd.add("file", 1);
        pd.add("package", 1);
        pd.add("application", 1);

        variables_map vm;
        store(command_line_parser(argc, argv).options(*options.description).positional(pd).allow_unregistered().run(), vm);
        notify(vm);

        if (vm.count("help"))
        {
            options.showHelp = true;
            return options;
        }

        if (vm.count("file"))
        {
            options.scxPath = vm["file"].as<std::string>();;
        }

        if (vm.count("command"))
        {
            options.command = vm["command"].as<std::string>();
            options.command = simox::alg::to_lower(options.command);

            if (options.command != "start"
                && options.command != "stop"
                && options.command != "kill"
                && options.command != "restart"
                && options.command != "list"
                && options.command != "generate"
                && options.command != "status"
                && options.command != "deploy"
                && options.command != "remove"
                && options.command != "periodic_status"
               )
            {
                options.error = true;
            }
        }

        if (vm.count("package"))
        {
            options.packageName = vm["package"].as<std::string>();
        }

        if (vm.count("application"))
        {
            options.applicationName = vm["application"].as<std::string>();
        }

        if (vm.count("parameters"))
        {
            options.parameters = vm["parameters"].as<std::string>();
        }
        if (vm.count("print"))
        {
            options.print = true;
        }
        if (vm.count("wait"))
        {
            options.wait = true;
        }
        if (vm.count("remote"))
        {
            options.remote = true;
        }

        if (options.scxPath.empty() || options.command.empty())
        {
            options.error = true;
        }

        return options;
    }

    void ScenarioManagerCliOptions::showHelp(CmdOptions options, std::string errorMessage)
    {
        std::cout << *options.description << std::endl;
        if (!errorMessage.empty())
        {
            std::cout << "\033[1m" << LogSender::GetColorCodeString(LogSender::eRed) << std::endl
                      << "Error: " << LogSender::GetColorCodeString(LogSender::eReset) << "\033[1m" << errorMessage << std::endl;
        }
    }

    std::string ScenarioManagerCliOptions::GetScenarioNameByCommandLineInput(CmdOptions options)
    {
        if (options.scxPath.empty())
        {
            return "";
        }

        std::filesystem::path boostScxPath(options.scxPath);

        if (std::filesystem::exists(boostScxPath))
        {
            if (std::filesystem::is_regular_file(boostScxPath))
            {
                return boostScxPath.stem().string();
            }
            else if (std::filesystem::is_directory(boostScxPath))
            {
                std::vector< std::string > all_matching_files;

                std::filesystem::directory_iterator end_itr; // Default ctor yields past-the-end
                for (std::filesystem::directory_iterator i(boostScxPath.string()); i != end_itr; ++i)
                {
                    // Skip if not a file
                    if (!std::filesystem::is_regular_file(i->status()))
                    {
                        continue;
                    }

                    if (!(i->path().extension() == ".scx"))
                    {
                        continue;
                    }

                    // File matches, store it
                    all_matching_files.push_back(i->path().stem().string());
                }

                if (all_matching_files.size() == 0)
                {
                    return "";
                }
                else if (all_matching_files.size() == 1)
                {
                    return all_matching_files[0];
                }
                else
                {
                    std::cout << "Warning: found multiple scx files in this directory choosing " << all_matching_files[0];
                    return all_matching_files[0];
                }
            }
            else
            {
                return "";
            }
        }
        else
        {
            //input should be an Scenario name now
            return options.scxPath;
        }
    }
}
