/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::PackagePath
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <exception>
#include <filesystem>
#include <string>

// ArmarX
#include <ArmarXCore/interface/core/PackagePath.h>


namespace armarx
{

    class PackageNotFoundException : public std::runtime_error
    {

    public:

        PackageNotFoundException(const std::string& package) :
            std::runtime_error("Package '" + package + "' could not be found.")
        {
            // pass
        }

    };

    class PackagePath
    {

    private:

        data::PackagePath m_pp;

    public:

        PackagePath(const std::string& package_name, const std::filesystem::path& rel_path);

        PackagePath(const data::PackagePath& pp);

        virtual ~PackagePath();

        static std::filesystem::path toSystemPath(const data::PackagePath& pp);

        std::filesystem::path toSystemPath() const;

        data::PackagePath serialize() const;

    };
}
