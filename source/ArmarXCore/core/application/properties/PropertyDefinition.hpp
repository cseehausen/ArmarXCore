/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#pragma once

#include <SimoxUtility/algorithm/get_map_keys_values.h>

#include <ArmarXCore/core/application/properties/PropertyDefinition.h>
#include <ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h>
#include <ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h>
#include <ArmarXCore/core/exceptions/local/UnmappedValueException.h>
#include <ArmarXCore/core/exceptions/local/ValueRangeExceededException.h>
#include <ArmarXCore/util/check.h>

// Ice
#include <IceUtil/Time.h>

#include <limits>
#include <type_traits>
#include <vector>

namespace armarx
{
    template<typename PropertyType>
    PropertyDefinition<PropertyType>::PropertyDefinition(
        PropertyType* setterRef,
        const std::string& propertyName,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness) :
        PropertyDefinitionBase(true, constness),
        propertyName(propertyName),
        description(description),
        setterRef{setterRef},
        factory(nullptr),
        regex(""),
        caseInsensitive(true),
        expandEnvVars(true),
        stringRemoveQuotes(true),
        max(std::numeric_limits<double>::max()),
        min(-std::numeric_limits<double>::max())
    {
        initHook();
    }


    template<typename PropertyType> inline
    PropertyDefinition<PropertyType>::PropertyDefinition(
        PropertyType* setterRef,
        const std::string& propertyName,
        PropertyType defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness) :
        PropertyDefinitionBase(false, constness),
        propertyName(propertyName),
        description(description),
        defaultValue(defaultValue),
        setterRef{setterRef},
        factory(nullptr),
        regex(""),
        caseInsensitive(true),
        expandEnvVars(true),
        stringRemoveQuotes(true),
        max(std::numeric_limits<double>::max()),
        min(-std::numeric_limits<double>::max())
    {
        initHook();
    }

    std::string PropertyDefinition_toLowerCopy(std::string const& input);


    template <typename PropertyType> inline
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::map(
        const std::string& valueString,
        PropertyType value)
    {
        if (caseInsensitive)
        {
            std::string lowerCaseValueString = PropertyDefinition_toLowerCopy(valueString);

            propertyValuesMap[lowerCaseValueString] = ValueEntry(valueString,
                    value);
        }
        else
        {
            propertyValuesMap[valueString] = ValueEntry(valueString, value);
        }

        return *this;
    }

    template <typename PropertyType> inline
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::map(
        const std::map<std::string, PropertyType>& values)
    {
        for (const auto& [name, value] : values)
        {
            map(name, value);
        }
        return *this;
    }

    template <typename PropertyType>
    template<class T> inline
    std::enable_if_t < std::is_same_v<T, PropertyType>&& !std::is_same_v<T, std::string>, PropertyDefinition<T>& >
    PropertyDefinition<PropertyType>::map(const std::map<T, std::string>& values)
    {
        for (const auto& [value, name] : values)
        {
            map(name, value);
        }
        return *this;
    }

    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setFactory(const PropertyFactoryFunction& func)
    {
        this->factory = func;

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setCaseInsensitive(bool caseInsensitive)
    {
        if (this->caseInsensitive != caseInsensitive)
        {
            // rebuild value map
            PropertyValuesMap newPropertyValuesMap;

            typename PropertyValuesMap::iterator valueIter =
                propertyValuesMap.begin();

            std::string key;

            while (valueIter != propertyValuesMap.end())
            {
                key = valueIter->second.first;

                if (caseInsensitive)
                {
                    key = PropertyDefinition_toLowerCopy(key);
                }

                newPropertyValuesMap[key] = valueIter->second;
                valueIter++;
            }

            // set the new map
            propertyValuesMap = newPropertyValuesMap;
            this->caseInsensitive = caseInsensitive;
        }

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setExpandEnvironmentVariables(bool expand)
    {
        this->expandEnvVars = expand;
        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setRemoveQuotes(bool removeQuotes)
    {
        this->stringRemoveQuotes = removeQuotes;
        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMatchRegex(
        const std::string& regex)
    {
        this->regex = regex;

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMin(double min)
    {
        if constexpr(std::is_arithmetic_v<PropertyType>)
        {
            this->min = min;

            return *this;
        }
        else
        {
            ARMARX_WARNING << "Cannot use setMin() on a non-arithmetic type.  Ignoring.";
            return *this;
        }
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMax(double max)
    {
        if constexpr(std::is_arithmetic_v<PropertyType>)
        {
            this->max = max;

            return *this;
        }
        else
        {
            ARMARX_WARNING << "Cannot use setMax() on a non-arithmetic type.  Ignoring.";
            return *this;
        }
    }

    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::isCaseInsensitive() const
    {
        return caseInsensitive;
    }


    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::expandEnvironmentVariables() const
    {
        return expandEnvVars;
    }


    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::removeQuotes() const
    {
        return stringRemoveQuotes;
    }


    template <typename PropertyType>
    std::string PropertyDefinition<PropertyType>::getMatchRegex() const
    {
        return regex;
    }


    template <typename PropertyType>
    double
    PropertyDefinition<PropertyType>::getMin() const
    {
        return min;
    }


    template <typename PropertyType>
    double
    PropertyDefinition<PropertyType>::getMax() const
    {
        return max;
    }


    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::getPropertyName() const
    {
        return propertyName;
    }


    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::getDescription() const
    {
        return description;
    }


    template <typename PropertyType>
    typename PropertyDefinition<PropertyType>::PropertyValuesMap&
    PropertyDefinition<PropertyType>::getValueMap()
    {
        return propertyValuesMap;
    }


    template <typename PropertyType>
    PropertyType
    PropertyDefinition<PropertyType>::getDefaultValue()
    {
        // throw exception if no default exists

        if (isRequired())
        {
            throw armarx::LocalException("Required property '" + getPropertyName() + "': default doesn't exist.");
        }

        return defaultValue;
    }


    template <typename PropertyType>
    typename PropertyDefinition<PropertyType>::PropertyFactoryFunction PropertyDefinition<PropertyType>::getFactory() const
    {
        return factory;
    }

    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::toString(PropertyDefinitionFormatter& formatter, const std::string& value)
    {
        std::vector<std::string> mapValues;

        typename PropertyValuesMap::iterator it = propertyValuesMap.begin();

        while (it != propertyValuesMap.end())
        {
            mapValues.push_back(it->second.first);

            ++it;
        }

        return formatter.formatDefinition(
                   propertyName,
                   description,
                   (min != -std::numeric_limits<double>::max() ? PropertyDefinition_lexicalCastToString(min) : ""),
                   (max != std::numeric_limits<double>::max() ? PropertyDefinition_lexicalCastToString(max) : ""),
                   getDefaultAsString(),
                   (!isCaseInsensitive() ? "yes" : "no"),
                   (isRequired() ? "yes" : "no"),
                   regex,
                   mapValues,
                   value);
    }


    template <typename PropertyType>
    std::string PropertyDefinition<PropertyType>::getDefaultAsString()
    {
        if (not isRequired())
        {
            if constexpr(meta::properties::DefaultAsStringPlugin<PropertyType>::value)
            {
                return meta::properties::DefaultAsStringPlugin<PropertyType>::Str(*this);
            }
            else if constexpr(std::is_same_v<PropertyType, IceUtil::Time>)
            {
                IceUtil::Time time = getDefaultValue();
                double time_count = time.toMicroSecondsDouble();
                std::string unit = "µs";

                if (time_count >= 1000)
                {
                    time_count /= 1000;
                    unit = "ms";

                    if (time_count >= 1000)
                    {
                        time_count /= 1000;
                        unit = "s";

                        if (time_count >= 60)
                        {
                            time_count /= 60;
                            unit = "min";

                            if (time_count >= 60)
                            {
                                time_count /= 60;
                                unit = "h";

                                if (time_count >= 24)
                                {
                                    return time.toDateTime();
                                }
                            }
                        }
                    }
                }

                return PropertyDefinition_lexicalCastToString(time_count) + " " + unit;
            }
            // bool
            else if constexpr(std::is_same_v<PropertyType, bool>)
            {
                return getDefaultValue() ? "true" : "false";
            }
            // floating point types
            else if constexpr(std::is_floating_point_v<PropertyType>)
            {
                return PropertyDefinition_lexicalCastToString(getDefaultValue());
            }
            // integral types
            else if constexpr(std::is_integral_v<PropertyType>)
            {
                return PropertyDefinition_lexicalCastToString(getDefaultValue());
            }
            // std::string
            else if constexpr(std::is_same_v<PropertyType, std::string>)
            {
                if (getDefaultValue().empty())
                {
                    return "\"\"";
                }

                return getDefaultValue();
            }
            // mappings
            else
            {
                typename PropertyValuesMap::iterator it = propertyValuesMap.begin();

                while (it != propertyValuesMap.end())
                {
                    if (it->second.second == getDefaultValue())
                    {
                        return it->second.first;
                    }

                    ++it;
                }

                return "Default value not mapped.";
            }
        }

        // no default available
        return std::string();
    }


    template <typename PropertyType>
    PropertyType
    PropertyDefinition<PropertyType>::getValue(const std::string& prefix, Ice::PropertiesPtr iceProperties)
    {
        return isRequired() ? getValueRequired(prefix, iceProperties) : getValueOptional(prefix, iceProperties);
    }


    template <typename PropertyType>
    void
    PropertyDefinition<PropertyType>::writeValueToSetter(const std::string& prefix, Ice::PropertiesPtr iceProperties)
    {
        if (setterRef != nullptr)
        {
            *setterRef = getValue(prefix, iceProperties);
        }
    }

    std::map<std::string, std::string> PropertyDefinition_parseMapStringString(std::string const& input);
    std::vector<std::string> PropertyDefinition_parseVectorString(std::string const& input);
    IceUtil::Time PropertyDefinition_parseIceUtilTime(std::string const& input);


    template <typename PropertyType>
    void
    PropertyDefinition<PropertyType>::initHook()
    {
        // bool
        if constexpr(std::is_same_v<PropertyType, bool>)
        {
            setCaseInsensitive(true);
            map("true", true);
            map("false", false);
            map("yes", true);
            map("no", false);
            map("1", true);
            map("0", false);
        }
        // armarx::MessageType
        else if constexpr(std::is_same_v<PropertyType, armarx::MessageTypeT>)
        {
            setCaseInsensitive(true);
            map("Debug", armarx::MessageTypeT::DEBUG);
            map("Verbose", armarx::MessageTypeT::VERBOSE);
            map("Info", armarx::MessageTypeT::INFO);
            map("Important", armarx::MessageTypeT::IMPORTANT);
            map("Warning", armarx::MessageTypeT::WARN);
            map("Error", armarx::MessageTypeT::ERROR);
            map("Fatal", armarx::MessageTypeT::FATAL);
            map("Undefined", armarx::MessageTypeT::UNDEFINED);
        }
        // std::map<std::string, std::string>
        else if constexpr(std::is_same_v<PropertyType, std::map<std::string, std::string>>)
        {
            setFactory(PropertyDefinition_parseMapStringString);
        }
        // std::vector<std::string>
        else if constexpr(std::is_same_v<PropertyType, std::vector<std::string>>)
        {
            setFactory(PropertyDefinition_parseVectorString);
        }
        // IceUtil::Time
        else if constexpr(std::is_same_v<PropertyType, IceUtil::Time>)
        {
            setCaseInsensitive(true);
            setFactory(PropertyDefinition_parseIceUtilTime);
        }
        else if constexpr(meta::properties::PDInitHookPlugin<PropertyType>::value)
        {
            meta::properties::PDInitHookPlugin<PropertyType>::Init(*this);
        }
    }


    template <typename PropertyType>
    PropertyType
    PropertyDefinition<PropertyType>::getValueRequired(const std::string& prefix, Ice::PropertiesPtr iceProperties)
    {
        using namespace armarx::exceptions;

        checkRequirement(prefix, iceProperties);

        const std::string keyValue = getPropertyValue(prefix, iceProperties);
        const std::string value = getRawPropertyValue(prefix, iceProperties);

        if (matchRegex(value))
        {
            // Try factory.
            if (getFactory())
            {
                try
                {
                    return getFactory()(value);
                }
                catch (const armarx::LocalException& e)
                {
                    throw local::InvalidPropertyValueException(prefix + getPropertyName(), value)
                            << e.getReason();
                }
                catch (const std::exception& e)
                {
                    throw local::InvalidPropertyValueException(prefix + getPropertyName(), value)
                            << "Unknown exception while constructing type out of value: "
                            << e.what();
                }
                catch (...)
                {
                    throw local::InvalidPropertyValueException(prefix + getPropertyName(), value)
                            << "Unknown exception while constructing type out of value.";
                }
            }

            // Try mapped values.
            const PropertyValuesMap& propertyValuesMap = getValueMap();

            if (!propertyValuesMap.empty())
            {
                auto valueIter = propertyValuesMap.find(keyValue);
                if (valueIter != propertyValuesMap.end())
                {
                    return processMappedValue(valueIter->second.second);
                }

                // Mapping exist but value not found.
                throw local::UnmappedValueException(prefix + getPropertyName(), value)
                        << "Mapped keys: " << simox::alg::get_keys(getValueMap());
            }

            // Try parsing raw value.
            // bool
            if constexpr(std::is_same_v<PropertyType, bool>)
            {
                return PropertyDefinition_lexicalCastTo<bool>(value);
            }
            // arithmetic types
            else if constexpr(std::is_arithmetic_v<PropertyType>)
            {
                try
                {
                    PropertyType numericValue = PropertyDefinition_lexicalCastTo<PropertyType>(value);
                    checkLimits(prefix, numericValue);
                    return numericValue;
                }
                catch (const std::bad_cast&)
                {
                    throw local::InvalidPropertyValueException(prefix + getPropertyName(), value);
                }
            }
            // string
            else if constexpr(std::is_same_v<PropertyType, std::string>)
            {
                return processMappedValue(value);
            }
            else
            {
                throw exceptions::local::UnmappedValueException(prefix + getPropertyName(), value)
                        << "Mapped keys: " << simox::alg::get_keys(getValueMap());
            }
        }

        throw local::InvalidPropertyValueException(
            prefix + getPropertyName(), getRawPropertyValue(prefix, iceProperties));
    }


    template <typename PropertyType>
    PropertyType
    PropertyDefinition<PropertyType>::getValueOptional(const std::string& prefix, Ice::PropertiesPtr iceProperties)
    {
        using namespace armarx::exceptions;

        if (!isSet(prefix, iceProperties))
        {
            return getDefaultValue();
        }
        else
        {
            return getValueRequired(prefix, iceProperties);
        }
    }


    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::getPropertyValue(const std::string& prefix, Ice::PropertiesPtr iceProperties) const
    {
        std::string value = icePropertyGet(iceProperties, prefix + getPropertyName());

        if (isCaseInsensitive())
        {
            value = PropertyDefinition_toLowerCopy(value);
        }

        return value;
    }


    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::getRawPropertyValue(const std::string& prefix, Ice::PropertiesPtr iceProperties) const
    {
        return icePropertyGet(iceProperties, prefix + getPropertyName());
    }

    bool PropertyDefinition_matchRegex(std::string const& regex, std::string const& value);


    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::matchRegex(const std::string& value) const
    {
        if (!getMatchRegex().empty())
        {
            return PropertyDefinition_matchRegex(getMatchRegex(), value);
        }

        return true;
    }


    template <typename PropertyType>
    void
    PropertyDefinition<PropertyType>::checkRequirement(const std::string& prefix, Ice::PropertiesPtr iceProperties)
    {
        using namespace armarx::exceptions;

        if (isRequired())
        {
            if (!isSet(prefix, iceProperties))
            {
                throw local::MissingRequiredPropertyException(prefix + getPropertyName(),
                        iceProperties);
            }
        }
    }


    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::isSet(const std::string& prefix, Ice::PropertiesPtr iceProperties) const
    {
        return PropertyDefinitionBase::isSet(prefix, getPropertyName(), iceProperties);
    }


    template <typename PropertyType>
    template <typename T>
    T
    PropertyDefinition<PropertyType>::processMappedValue(const T& value)
    {
        return value;
    }


    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::processMappedValue(const std::string& value)
    {
        std::string result = value;
        if (expandEnvironmentVariables())
        {
            expandEnvironmentVariables(result, result);
        }
        if (removeQuotes())
        {
            removeQuotes(result, result);
        }
        return result;
    }


    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::expandEnvironmentVariables(const std::string& input, std::string& output)
    {
        output = input;

        if (input == "")
        {
            return false;
        }

        bool res = true;
        bool goOn = false;

        do
        {
            size_t foundStart = output.find("${");

            if (foundStart != std::string::npos)
            {
                goOn = true;
                size_t foundEnd = output.find("}", foundStart);

                if (foundEnd != std::string::npos)
                {
                    std::string envVar = output.substr(foundStart + 2, foundEnd - foundStart - 2);
                    std::string replacement;
                    char* envVarENV = getenv(envVar.c_str());

                    if (envVarENV)
                    {
                        replacement = envVarENV;
                    }
                    else
                    {
                        std::cout << "WARNING: Could not expand environment variable: " << envVar << std::endl;
                    }

                    output.replace(foundStart, foundEnd - foundStart + 1, replacement);
                }
                else
                {
                    std::cout << "ERROR: Found '${' but missing '}' in " << input << std::endl;
                    goOn = false;
                    res = false;
                }

            }
            else
            {
                goOn = false;
            }

        }
        while (goOn);

        return res;
    }


    template <typename PropertyType>
    void
    PropertyDefinition<PropertyType>::removeQuotes(const std::string& input, std::string& output)
    {
        output = input;

        if (input.size() < 2)
        {
            return;
        }

        if ((input[0] == '"' && input[input.size() - 1] == '"')
            || (input[0] == '\'' && input[input.size() - 1] == '\'')
           )
        {
            output = input.substr(1, input.size() - 2);
        }
    }


    template <typename PropertyType>
    void
    PropertyDefinition<PropertyType>::checkLimits(const std::string& prefix, PropertyType numericValue) const
    {
        using namespace armarx::exceptions;

        double dirtyValue = PropertyDefinition_lexicalCastTo<double>(
                                PropertyDefinition_lexicalCastToString(numericValue));

        if ((dirtyValue < getMin()) || (dirtyValue > getMax()))
        {
            throw local::ValueRangeExceededException(
                prefix + getPropertyName(),
                getMin(),
                getMax(),
                dirtyValue);
        }
    }
}
