/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

// Slice Includes
#include <ArmarXCore/interface/statechart/RemoteStateOffererIce.h>

// ArmarX Includes
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/ArmarXManager.h>

// Statechart Includes
#include "State.h"
#include "RemoteState.h"
#include "RemoteStateWrapper.h"
#include "StatechartManager.h"

#include <type_traits>

namespace armarx
{
    std::string joinStrings(std::vector<std::string> const& input, std::string const& seperator);

    struct RemoteStateOffererBase
        : virtual RemoteStateOffererIceBase
        , virtual State
    {
        //! \brief Implement this function to specify the RemoteStateOfferer prefix.
        //! \note Do not override getDefaultName()!
        virtual std::string getStateOffererName() const = 0;
        //! \brief Pure virtual function, in which the states must be added, that should be remote-accessable.
        virtual void onInitRemoteStateOfferer() = 0;
        //! \brief Virtual function, in which the user can fetch some proxies.
        virtual void onConnectRemoteStateOfferer();
        //! \brief Virtual function, in which the user can implement some clean up.
        virtual void onExitRemoteStateOfferer();


        virtual void waitUntilComponentStarted() = 0;

        // inherited from RemoteStateOffererInterface
        CreateRemoteStateInstanceOutput createRemoteStateInstanceNew(
            CreateRemoteStateInstanceInput const& input,
            Ice::Current const& context) override;

        int createRemoteStateInstance(
            const std::string& stateName,
            const RemoteStateIceBasePrx& remoteStatePrx,
            const std::string& parentStateItentifierStr,
            const std::string& instanceName,
            const Ice::Current& context = Ice::emptyCurrent) override;

        void updateGlobalStateIdRecursive(
            int stateId,
            const std::string& parentId,
            const Ice::Current& context = Ice::emptyCurrent) override;

        void callRemoteState(
            int stateId,
            const StringVariantContainerBaseMap& properties,
            const Ice::Current& context = Ice::emptyCurrent) override;

        void exitRemoteState(
            int stateId,
            const ::Ice::Current& context = Ice::emptyCurrent) override;

        bool breakRemoteState(
            int stateId,
            const EventBasePtr& evt,
            const ::Ice::Current& context = Ice::emptyCurrent) override;

        bool isRemoteStateFinished(
            int stateId,
            const ::Ice::Current& context = Ice::emptyCurrent) override;

        bool breakActiveSubstateRemotely(
            int stateId,
            const EventBasePtr& evt,
            const ::Ice::Current& context = Ice::emptyCurrent) override;

        void notifyEventBufferedDueToUnbreakableStateRemote(int stateId, bool eventBuffered, const ::Ice::Current& context = Ice::emptyCurrent) override;
        StateIceBasePtr refetchRemoteSubstates(int stateId, const ::Ice::Current& context = Ice::emptyCurrent) override;
        StateParameterMap getRemoteInputParameters(const std::string& stateName, const ::Ice::Current& context = Ice::emptyCurrent) override;
        StateParameterMap getRemoteOutputParameters(const std::string& stateName, const ::Ice::Current& context = Ice::emptyCurrent) override;
        StateParameterMap getRemoteInputParametersById(int stateId, const ::Ice::Current& context = Ice::emptyCurrent) override;
        StateParameterMap getRemoteOutputParametersById(int stateId, const ::Ice::Current& context = Ice::emptyCurrent) override;

        bool hasSubstatesRemote(const std::string& stateName, const ::Ice::Current& context = Ice::emptyCurrent) const override;
        bool hasActiveSubstateRemote(int stateId, const ::Ice::Current& context = Ice::emptyCurrent) override;
        Ice::StringSeq getAvailableStates(const ::Ice::Current& context = Ice::emptyCurrent) override;
        StateIdNameMap getAvailableStateInstances(const ::Ice::Current& context = Ice::emptyCurrent) override;
        StateIceBasePtr getStatechart(const std::string& stateName, const ::Ice::Current& context = Ice::emptyCurrent) override;
        StateIceBasePtr getStatechartInstance(int stateId, const Ice::Current& = Ice::emptyCurrent) override;
        StateIceBasePtr getStatechartInstanceByGlobalIdStr(const std::string& globalStateIdStr, const Ice::Current& = Ice::emptyCurrent) override;
        bool isHostOfStateByGlobalIdStr(const std::string& globalStateIdStr, const Ice::Current& = Ice::emptyCurrent) override;

        void removeInstance(int stateId, const Ice::Current& = Ice::emptyCurrent) override;
        void issueEvent(int receivingStateId, const EventBasePtr& evt, const Ice::Current& = Ice::emptyCurrent) override;
        void issueEventWithGlobalIdStr(const std::string& globalStateIdStr, const EventBasePtr& evt, const Ice::Current& = Ice::emptyCurrent) override;

        //! \brief Overridden so that the user cannot use it
        RemoteStatePtr addRemoteState(std::string stateName, std::string proxyName, std::string instanceName) override;
        //! \brief Overridden so that the user cannot use it
        RemoteStatePtr addDynamicRemoteState(std::string instanceName) override;


        /** \struct RemoteStateData
          A struct that holds meta data for a remoteaccessable state instances.
          */
        struct RemoteStateData
        {
            //! Local id of this RemoteStateOfferer, that identifies the state instance in the stateInstanceList
            int id;
            //! Not used yet.
            std::string callerIceName;
            //! Proxy to the state, that called this state
            RemoteStateIceBasePrx  callerStatePrx;
            //! Pointer to a Pseudo parent state, that contains the real state instance.
            RemoteStateWrapperPtr remoteWrappedState;

        };
        RemoteStateData getInstance(int stateId);
        StateBasePtr getGlobalInstancePtr(int globalId) const;
        virtual StateBasePtr getStatePtr(const std::string& stateName) const;
        std::map<int, StateBasePtr> getChildStatesByName(int parentId, std::string stateName);

        StateIceBasePtr getStatechartInstanceByGlobalIdStrRecursive(const std::string& globalStateIdStr, StateBasePtr state, int& stateCounter);

        void onConnectStatechartImpl();
        void onExitStatechartImpl();
        void run() override;

        void initState(State& state);



        HiddenTimedMutex stateInstanceListMutex;
        //! Holds the instances that where requested from remotely located states
        std::map<int, RemoteStateData> stateInstanceList;
        std::string componentName;
    };

    /**
      \class RemoteStateOfferer
      \brief Class that holds states, which offer functionality for other states over Ice.
      \ingroup StatechartGrp
      To offer such functionality one must derive from this class and implement
      the function armarx::RemoteStateOfferer::onInitRemoteStateOfferer() and add the states offering remote functionality there.<br/>
      For every incoming call to a state a copy of the base instance is created, so that concurrent calls dont interfere with each other.<br/>
      The implemented, remoteaccessable states must call setRemoteaccessable().<br/>
      An Example can be found in applications/StateChartExamples/RemoteAccessableStateExample.

      \tparam ContextType The template parameter must be a class that derives from
             StatechartContext

      \see StatechartContext, RemoteState, DynamicRemoteState, RemoteStateWrapper

      */
    template <typename ContextType = StatechartContext>
    struct RemoteStateOfferer
        : virtual RemoteStateOffererBase
        , virtual ContextType
    {
        void waitUntilComponentStarted() override;

        static_assert(std::is_base_of_v<StatechartContext, ContextType>,
                      "The template parameter of RemoteStateOfferer, must be a class that derives from StatechartContext or StatechartContext itself");

        std::string getDefaultName() const override
        {
            return getStateOffererName() + "StateOfferer";
        }

        void onInitStatechart() override;
        void onConnectStatechart() override;
        void onExitStatechart() override;
    };


    /////////////////////////////////////////////////////////////
    ////////////// Implementation
    /////////////////////////////////////////////////////////////


    template <typename ContextType>
    void RemoteStateOfferer<ContextType>::onInitStatechart()
    {
        stateName = getDefaultName();
        ContextType::setToplevelState(this);

        StatePhase oldPhase = getStatePhase();
        setStatePhase(eSubstatesDefinitions);


        try
        {
            onInitRemoteStateOfferer();
        }
        catch (...)
        {
            setStatePhase(oldPhase);
            Component::terminate();
            throw;
        }

        for (StateIceBasePtr state : subStateList)
        {
            StateControllerPtr stateController = StateControllerPtr::dynamicCast(state);

            if (stateController)
            {
                int numberLogLevels = Component::getProperty<int>("ProfilingDepth").getValue();
                stateController->addProfilerRecursive(Component::getProfiler(), numberLogLevels);
                stateController->addProfilerRecursive(StatechartContext::stateReporter, -1);
            }
        }

        ContextType::setAutoEnterToplevelState(false);
    }

    template<typename ContextType>
    void RemoteStateOfferer<ContextType>::onConnectStatechart()
    {
        onConnectStatechartImpl();
    }

    template<typename ContextType>
    void RemoteStateOfferer<ContextType>::onExitStatechart()
    {
        onExitStatechartImpl();
    }

    template<typename ContextType>
    void RemoteStateOfferer<ContextType>::waitUntilComponentStarted()
    {
        if (!ContextType::getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted, 5000))
        {
            throw LocalException() << "Cannot create a remote state instance because the RemoteStateOfferer '"
                                   << ContextType::getName()
                                   << "' is still waiting for dependencies: "
                                   << joinStrings(ContextType::getUnresolvedDependencies(), ", ");
        }
    }

}
