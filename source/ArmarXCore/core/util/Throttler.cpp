#include "Throttler.h"

namespace armarx
{
    Throttler::Throttler(const float frequency)
        : delay(1 / frequency * 1'000'000) {}

    bool Throttler::check(const int64_t& timestamp) noexcept
    {
        const int64_t timeSinceLastTrue = timestamp - lastTimeTrue;

        if (timeSinceLastTrue >= delay)
        {
            lastTimeTrue = timestamp;
            return true;
        }

        return false;
    }

} // namespace armarx