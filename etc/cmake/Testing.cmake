# macros for testing

find_package(Lcov)

# Option to en-/disable unittests
option(ARMARX_BUILD_TESTS "Build tests" ON)

# Option to en-/disable generation of gcov reports
option(ARMARX_ENABLE_COVERAGE "Enable the creation of gcov coverage reports" OFF)
if("${ARMARX_ENABLE_COVERAGE}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} --coverage")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage" )
    set(ARMARX_BUILD_TESTS ON)
endif()


##
## This macro creates the Test.h file which is required to collect test results on Jenkins.
## The location for the file is testing/<ProjectInstallName> inside the build directory
##
macro(generate_test_h)
    set(ARMARX_TEST_H_DIRECTORY "${PROJECT_BINARY_DIR}/testing/${ARMARX_PROJECT_NAME}")

    # if the required build/testing/<ProjectInstallName> directory is not
    if (NOT EXISTS "${ARMARX_TEST_H_DIRECTORY}")
        file(MAKE_DIRECTORY "${ARMARX_TEST_H_DIRECTORY}")
    endif()

    # include the directory
    include_directories("${PROJECT_BINARY_DIR}/testing/")

    # export all CMake variables into VARIABLE_LIST_CODE which is then exported into
    # the Test.h file
    # This makes it possible to access all CMake variables like inside Tests
    get_cmake_property(_variableNames VARIABLES)
    set(VARIABLE_LIST_CODE "")
    foreach (_variableName ${_variableNames})
        STRING(REGEX REPLACE "\\\\" "\\\\\\\\" _variableValue "${${_variableName}}")
        STRING(REGEX REPLACE "\n" "\\\\n" _variableValue "${_variableValue}")
        STRING(REGEX REPLACE "\"" "\\\\\"" _variableValue "${_variableValue}")
        set(VARIABLE_LIST_CODE "${VARIABLE_LIST_CODE}${_variableName}=${_variableValue}\n")
    endforeach()
    file(WRITE "${ARMARX_TEST_H_DIRECTORY}/cmakevars.cfg" "${VARIABLE_LIST_CODE}")
    # create Test.h file which must be included by all testbench files
    configure_file("${ArmarXCore_TEMPLATES_DIR}/ComponentTemplate/Test.h.in" "${ARMARX_TEST_H_DIRECTORY}/Test.h")
endmacro()

if( ARMARX_BUILD_TESTS )
    if(NOT Boost_UNIT_TEST_FRAMEWORK_LIBRARY)
        message(STATUS "Searching boost test")
        find_package(Boost ${ArmarX_BOOST_VERSION} EXACT REQUIRED COMPONENTS unit_test_framework)
    endif()

    if(NOT Boost_UNIT_TEST_FRAMEWORK_LIBRARY)
        message(SEND_ERROR "Boost::Test was not found. Can not build unit tests.")
    endif()

    enable_testing()

    generate_test_h()

    # add the "coverage" custom target which produces an html coverage report via genhtml
    if(Lcov_FOUND AND ${ARMARX_ENABLE_COVERAGE})
        set(COVERAGE_OUTPUT_DIR "${CMAKE_BINARY_DIR}/coverage")
        set(COVERAGE_OUTPUT_FILE "${CMAKE_BINARY_DIR}/coverage.lcov.info")

        # Hack: execute "CMAKE_BUILD_TOOL test" manually
        # it is not possible to add a dependency to the test target since it is reported as not existing
        add_custom_target(coverage-reset
            COMMAND "${Lcov_EXECUTABLE}" --directory "${CMAKE_BINARY_DIR}" --zerocounters
            COMMAND "${CMAKE_BUILD_TOOL}" test
            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
            COMMENT "Reset Code Coverage counters for ${PROJECT_NAME} and run tests.")

        add_custom_target(coverage
              COMMAND "${Lcov_EXECUTABLE}" --capture --base-directory "${CMAKE_BINARY_DIR}" --directory "${CMAKE_BINARY_DIR}" --output-file "${COVERAGE_OUTPUT_FILE}"
              COMMAND "${Lcov_GenHtml_EXECUTABLE}"  --show-details --output-directory "${COVERAGE_OUTPUT_DIR}" "${COVERAGE_OUTPUT_FILE}"
              WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
              COMMENT "Generating Code Coverage report for ${PROJECT_NAME}")
        add_dependencies(coverage coverage-reset)
    else()
        add_custom_target(coverage-reset
            COMMENT "Lcov has not been found.")

        add_custom_target(coverage
            COMMENT "Lcov has not been found.")
    endif()
endif( ARMARX_BUILD_TESTS )


##
## This macro adds a testcase to the testsuite which is run by the command 'make test'.
## All test executables are stored in ${ARMARX_BIN_DIR}.
## The Output path of the log files is stored in Test.h which gets generated above.
##
## PARAM TEST_NAME name of the test and the executable which gets created
## PARAM TEST_FILE name of the cpp file containing the test code
## PARAM DEPENDENT_LIBRARIES the libraries which must be linked to the testcase executable
##
macro(armarx_add_test)
    #there are two modes:
    # 1. legacy mode (3 params TEST_NAME + TEST_FILE + DEPENDENT_LIBS)
    # 2. parsing mode
    if("${ARGC}" EQUAL 3)
        set(TEST_NAME       "${ARGV0}")
        set(TEST_FILE       "${ARGV1}")
        set(DEPENDEND_LIBS  "${ARGV2}")
    else()
        #assume parsing mode
        set(multi_param
            DEPENDEND_LIBS
            LIBS
            ARON_FILES
            INCLUDE_DIRECTORIES)
        set(single_param
            TEST_NAME
            TEST_FILE)
        set(flag_param)
        cmake_parse_arguments(PREFIX "${flag_param}" "${single_param}" "${multi_param}" ${ARGN})

        if(PREFIX_DEPENDEND_LIBS)
            list(APPEND DEPENDEND_LIBS ${PREFIX_DEPENDEND_LIBS})
        endif()
        if(PREFIX_LIBS)

            list(APPEND DEPENDEND_LIBS ${PREFIX_LIBS})
        endif()

        if(PREFIX_ARON_FILES)
            set(ARMARX_ARON_FILES ${PREFIX_ARON_FILES})
        endif()

        if(PREFIX_INCLUDE_DIRECTORIES)
            set(ARMARX_INCLUDE_DIRECTORIES ${PREFIX_INCLUDE_DIRECTORIES})
        endif()

        if(PREFIX_TEST_NAME)
            set(TEST_NAME ${PREFIX_TEST_NAME})
        endif()

        if(PREFIX_TEST_FILE)
            set(TEST_FILE ${PREFIX_TEST_FILE})
        endif()
        if(PREFIX_UNPARSED_ARGUMENTS)
            message(FATAL_ERROR "${TEST_NAME}: encountered unparsed arguments! ${PREFIX_UNPARSED_ARGUMENTS}\nARGN = ${ARGN}")
        endif()
    endif()
    if (ARMARX_BUILD_TESTS)
        set(TEST_LINK_LIBRARIES ${DEPENDEND_LIBS})
        if(NOT Boost_UNIT_TEST_FRAMEWORK_LIBRARY)
            find_package(Boost ${ArmarX_BOOST_VERSION} EXACT REQUIRED COMPONENTS unit_test_framework)
        endif()
        if(NOT ("${TEST_LINK_LIBRARIES}" MATCHES "${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}"))
            list(APPEND TEST_LINK_LIBRARIES "${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}")
        endif()

        armarx_add_executable(${TEST_NAME} ${TEST_FILE} "${TEST_LINK_LIBRARIES}" NO_INSTALL)
        list(LENGTH ARMARX_ARON_FILES ARMARX_ARON_FILES_LENGTH)
        if(${ARMARX_ARON_FILES_LENGTH})
            armarx_enable_aron_file_generation_for_target(
                TARGET_NAME
                    ${TEST_NAME}
                ARON_FILES
                    ${ARMARX_ARON_FILES}
                #ENABLE_DEBUG_INFO
            )
        endif()

        list(LENGTH ARMARX_INCLUDE_DIRECTORIES ARMARX_INCLUDE_DIRECTORIES_LENGTH)
        if(${ARMARX_INCLUDE_DIRECTORIES_LENGTH})
            target_include_directories(${TEST_NAME} SYSTEM PUBLIC ${ARMARX_INCLUDE_DIRECTORIES})
        endif()

        get_target_property(DISABLE_TEST ${TEST_NAME} EXCLUDE_FROM_ALL)
        if (NOT DISABLE_TEST)
            message(STATUS "    Building test ${TEST_NAME}")
            add_test(NAME ${TEST_NAME}
                 COMMAND "${ARMARX_BIN_DIR}/${TEST_NAME}" --output_format=XML --log_level=all --report_level=detailed)
        endif()
    endif()
endmacro()

