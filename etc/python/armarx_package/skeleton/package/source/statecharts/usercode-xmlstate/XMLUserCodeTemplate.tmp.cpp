/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::@LIBRARY_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "@COMPONENT_NAME@.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx::@LIBRARY_NAME@
{
    // DO NOT EDIT NEXT LINE
    @COMPONENT_NAME@::SubClassRegistry @COMPONENT_NAME@::Registry(@COMPONENT_NAME@::GetName(), &@COMPONENT_NAME@::CreateInstance);

    void @COMPONENT_NAME@::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
    }

    //void @COMPONENT_NAME@::run()
    //{
    //    // put your user code for the execution-phase here
    //    // runs in seperate thread, thus can do complex operations
    //    // should check constantly whether isRunningTaskStopped() returns true
    //
    //    // get a private kinematic instance for this state of the robot (tick "Robot State Component" proxy checkbox in statechart group)
    //    VirtualRobot::RobotPtr robot = getLocalRobot();
    //
    //// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //        // synchronize robot clone to most recent state
    //        RemoteRobot::synchronizeLocalClone(robot, getRobotStateComponent());
    //    }
    //}

    //void @COMPONENT_NAME@::onBreak()
    //{
    //    // put your user code for the breaking point here
    //    // execution time should be short (<100ms)
    //}

    void @COMPONENT_NAME@::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr @COMPONENT_NAME@::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new @COMPONENT_NAME@(stateData));
    }
}
