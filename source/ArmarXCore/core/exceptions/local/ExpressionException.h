/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <SimoxUtility/math/compare/is_equal.h>
#include <SimoxUtility/math/compare/is_greater.h>
#include <SimoxUtility/math/compare/is_greater_equal.h>
#include <SimoxUtility/math/compare/is_inequal.h>
#include <SimoxUtility/math/compare/is_less.h>
#include <SimoxUtility/math/compare/is_less_equal.h>

#include <ArmarXCore/util/CPPUtility/Pointer.h>
#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <string>


namespace armarx::exceptions::local
{
    /**
      \class ExpressionException
      \brief This exception is thrown if the macro \ref ARMARX_CHECK_EXPRESSION
      is used.
      \ingroup Exceptions
     */
    class ExpressionException : public armarx::LocalException
    {
    public:
        ExpressionException(std::string expression) noexcept :
            armarx::LocalException("The following expression is not true, but needs to be: '" + expression + "'"),
            expression(expression)
        { }

        std::string name() const override
        {
            return "armarx::exceptions::local::ExpressionException";
        }

        std::string getExpression() const
        {
            return expression;
        }

    private:
        std::string expression;
    };

}

/**
\ingroup Exceptions
\brief This macro evaluates the expression and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_EXPRESSION(expression)                                     \
    if( !(expression) )                                                         \
        throw ::armarx::exceptions::local::ExpressionException(#expression)     \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in "             \
                << __FUNCTION__ << " in )\n"

/**
\ingroup Exceptions
\brief Shortcut for ARMARX_CHECK_EXPRESSION
  **/
#define ARMARX_CHECK(expression) ARMARX_CHECK_EXPRESSION(expression)

/**
\ingroup Exceptions
\brief This macro evaluates the expression (pred(lhs, rhs)) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_BINARY_PREDICATE(lhs, rhs, pred)                                               \
    if( !(pred ( lhs , rhs) ) )                                                                     \
        throw ::armarx::exceptions::local::ExpressionException(#pred " ( "#lhs ", "  #rhs " )")     \
                << "\n(lhs = " << lhs << ", rhs = " << rhs << ")"                                   \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"

/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is less (<) than rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_LESS(lhs, rhs)          ARMARX_CHECK_BINARY_PREDICATE(lhs, rhs, simox::math::is_less)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is less or equal (<=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_LESS_EQUAL(lhs, rhs)    ARMARX_CHECK_BINARY_PREDICATE(lhs, rhs, simox::math::is_less_equal)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is greater (>) than rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_GREATER(lhs, rhs)       ARMARX_CHECK_BINARY_PREDICATE(lhs, rhs, simox::math::is_greater)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is greater or equal (>=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_GREATER_EQUAL(lhs, rhs) ARMARX_CHECK_BINARY_PREDICATE(lhs, rhs, simox::math::is_greater_equal)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is equal (==) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_EQUAL(lhs, rhs)         ARMARX_CHECK_BINARY_PREDICATE(lhs, rhs, simox::math::is_equal)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is inequal (!=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_NOT_EQUAL(lhs, rhs)     ARMARX_CHECK_BINARY_PREDICATE(lhs, rhs, simox::math::is_inequal)

/**
\ingroup Exceptions
\brief This macro evaluates whether number is positive (> 0) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_POSITIVE(number)                   ARMARX_CHECK_GREATER(number, 0)

/**
\ingroup Exceptions
\brief Check whether number is nonnegative (>= 0). If it is not, throw an
ExpressionException with the expression converted into a string.
  **/
#define ARMARX_CHECK_NONNEGATIVE(number)                ARMARX_CHECK_GREATER_EQUAL(number, 0)

/**
\ingroup Exceptions
\brief Check whether number is nonnegative (>= 0) and less than size. If it is not,
throw an ExpressionException with the expression converted into a string.
  **/
#define ARMARX_CHECK_FITS_SIZE(number, size)    \
    ARMARX_CHECK_NONNEGATIVE(number);           \
    ARMARX_CHECK_LESS(size_t(number), size)

/**
 * \ingroup Exceptions
 * \brief Check whether `lhs` is close to `rhs`, i.e. whether the absolute
 * difference s not more than `prec`.
 * \throw an ExpressionException with the expression converted into a string.
 */
#define ARMARX_CHECK_CLOSE(lhs, rhs, prec)  \
    if( !(std::abs(lhs - rhs) <= prec) )                                                           \
        throw ::armarx::exceptions::local::ExpressionException("| " #lhs " - " #rhs "| <= " #prec) \
                << "\n(lhs = " << lhs << ", rhs = " << rhs << ", prec = " << prec << ")"               \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"


/**
\ingroup Exceptions
\brief This macro evaluates whether number is finite (not nan or inf) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_FINITE(number)                                                                 \
    if( !(std::isfinite(number)) )                                                                  \
        throw ::armarx::exceptions::local::ExpressionException("std::isfinite(" #number ")")        \
                << "\n(number = " << number << ")"                                                  \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"

/**
\ingroup Exceptions
\brief This macro evaluates whether ptr is null and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_IS_NULL(ptr)                                                                   \
    if( ! ::armarx::isNullptr(ptr) )                                                                \
        throw ::armarx::exceptions::local::ExpressionException("ptr == nullptr")                    \
                << "\n(ptr = " << ptr << ")"                                                        \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"

/**
\ingroup Exceptions
\brief This macro evaluates whether ptr is not null and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_NOT_NULL(ptr)                                                                  \
    if( ::armarx::isNullptr(ptr) )                                                                  \
        throw ::armarx::exceptions::local::ExpressionException("ptr != nullptr")                    \
                << "\n(ptr = nullptr)"                                                              \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"

#define ARMARX_CHECK_NULL(ptr)                                                                      \
    if(! ::armarx::isNullptr(ptr) )                                                                 \
        throw ::armarx::exceptions::local::ExpressionException("ptr != nullptr")                    \
                << "\n(ptr = nullptr)"                                                              \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"

#define ARMARX_CHECK_EMPTY(c)                                                                       \
    if( ! c.empty() )                                                                               \
        throw ::armarx::exceptions::local::ExpressionException("c was expected to be empty")        \
                << "\n(c = " << c   << ")"                                                          \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"

#define ARMARX_CHECK_NOT_EMPTY(c)                                                                   \
    if( c.empty() )                                                                                 \
        throw ::armarx::exceptions::local::ExpressionException("c was not expected to be empty")    \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"

#define ARMARX_CHECK_MULTIPLE_OF(val, mod)                                                          \
    if( (val % mod) != 0 )                                                                          \
        throw ::armarx::exceptions::local::ExpressionException("val was expected to be a multiple of mod")  \
                << "\n(val = " << val << ", mod = " << mod << ")"                                   \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"\

#define ARMARX_CHECK_NOT_MULTIPLE_OF(val, mod)                                                      \
    if( (val % mod) == 0 )                                                                          \
        throw ::armarx::exceptions::local::ExpressionException("val was not expected to be a multiple of mod")  \
                << "\n(val = " << val << ", mod = " << mod << ")"                                   \
                <<"\n(at " << __FILE__ << ':' << __LINE__ << " in " << __FUNCTION__ << " in )\n"
/**
\ingroup Exceptions
\brief This macro evaluates the expression and if it turns out to be
false it will throw an exception of the given type.
  **/
#define ARMARX_CHECK_AND_THROW(expression, ExceptionType) do { \
        if( !(expression) ) \
            throw ExceptionType(#expression); \
    } while(0);

#undef eigen_assert
#define eigen_assert(expression) ARMARX_CHECK_EXPRESSION(expression)
