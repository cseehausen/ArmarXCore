/**
\page ArmarXCore-Tutorials-ice-remote-deployment Ice Remote Deployment

\section ArmarXCore-Tutorials-ice-remote-deployment-armarx-sync Creating an Sync folder using armarx-dev sync

In order to execute an Scenario remotely you need to ensure that all Pc's have the same build status.
To ensure this the armarx-dev CLI has an option to do this called sync

Example sync command:
\code
armarx-dev sync [optional --localSyncDir /path/to/local/sync/dir] --remoteSyncDir /path/to/remote/sync/dir --deployHosts i61pcXYZ,i61pcABC ArmarXCore ArmarXGui
\endcode

This will synchronize the data of all given hosts at the specified remoteSyncDir of all given packages.

Here is a list of all Option this Tool has:
\li --port PORT, -p PORT  \n server port (default: 22)
\li --deployHosts DEPLOYHOSTS, -d DEPLOYHOSTS \n remote machines to synchronize to (default: 10.4.1.1,10.4.1.2)
\li --localSyncDir LOCALSYNCDIR, -l LOCALSYNCDIR \n files are installed in this directory and then synchronized (default: /common/homes/students/your-username/sync-armarx)
\li --no-delete, -nd \n do not delete the localSyncDir content before installing (default: False)
\li --no-dep \n only install locally the given package without dependencies. Local install-directory will NOT be wiped before installing. (default: False)
\li --remoteSyncDir REMOTESYNCDIR, -r REMOTESYNCDIR \n directory on the remote host to synchronize to (can be set via environment variable ARMARX_REMOTE_SYNC_DIR) (default: armarx_username)
\li --username USERNAME, -u USERNAME \n user for connecting to remote machine (default: armar-username)
\li --compile, -c \n compile project before installing (default: False)
\li --ninja, -n \n Use ninja instead of make (default: False)
\li -j JOBCOUNT \n Number of cores to use for compilation. Number of cores of cpu is used of omitted. (default: 0)

\section ArmarXCore-Tutorials-ice-remote-deployment-remote-launching Launching an Scenario Remotly

After you have an an Sync folder on every Pc you have to make sure that your sync folder in your Default path curresponds to the path you used in the sync command

The default path to the file is : ~/.armarx/icegrid-sync-variables.icegrid.xml
and should look like this
\code
<icegrid>
        <variable name="ARMARX_SYNC_DIR" value="/absolute/path/to/your/sync/dir"/>
</icegrid>
\endcode

If both these Requirements are met you can either use \ref armarx-cli-scenario "ScenarioManagerCLI" or \ref ArmarXGui-GuiPlugins-ScnearioManagerGuiPlugin "ScenarioManagerGUI" to launch the Scenario remotely

*/
