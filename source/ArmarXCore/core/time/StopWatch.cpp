/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2019, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */


#include <ArmarXCore/core/time/StopWatch.h>


// STD/STL
#include <exception>

// Ice
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>


namespace
{
    /**
     * @brief An invalid timestamp.
     */
    const IceUtil::Time timestamp_invalid = IceUtil::Time::milliSeconds(-1);
}

namespace armarx
{
    StopWatch::StopWatch(TimeMode timeMode) :
        m_time_mode{timeMode},
        m_starting_time{TimeUtil::GetTime(m_time_mode)},
        m_time{::timestamp_invalid}
    {
        // pass
    }


    StopWatch::~StopWatch()
    {
        // pass
    }


    IceUtil::Time
    StopWatch::measure(std::function<void(void)> subjectToMeasure, TimeMode timeMode)
    {
        StopWatch sw{timeMode};
        subjectToMeasure();
        return sw.stop();
    }


    IceUtil::Time
    StopWatch::stop()
    {
        m_time = TimeUtil::GetTimeSince(m_starting_time, m_time_mode);
        return m_time;
    }


    void
    StopWatch::reset()
    {
        m_starting_time = TimeUtil::GetTime(m_time_mode);
        m_time = ::timestamp_invalid;
    }


    bool
    StopWatch::isStopped() const
    {
        return m_time != ::timestamp_invalid;
    }


    IceUtil::Time
    StopWatch::startingTime() const
    {
        return m_starting_time;
    }


    IceUtil::Time
    StopWatch::stoppingTime() const
    {
        if (not isStopped())
        {
            throw std::logic_error{"Timer was not stopped yet, cannot assess stopping time."};
        }

        return m_starting_time + m_time;
    }
}
