/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "TemplateMetaProgramming.h"
#include <vector>
#include <map>

namespace armarx
{
    /**
     * @brief This class is pretty much similar to a map.
     *
     * This class stores keys and values in two vectors and uses a map to map the keys to the index of the corresponding value
     * This enables map acces and index access.
     * The index of an inserted element never changes (this is different to a map).
     * Index access may be required in a high frequency case (e.g. an rt control loop)
     */
    template < class KeyT, class ValT,
               class KeyContT = std::vector<KeyT>,
               class ValContT = std::vector<ValT>,
               class IdxT = typename ValContT::size_type,
               class MapT = std::map<KeyT, IdxT >>
    class KeyValueVector
    {
    public:
        void            add(KeyT key, ValT value);
        IdxT            index(const KeyT& k) const;
        IdxT            size() const;

        ValT&           at(IdxT i);
        ValT     const& at(IdxT i) const;
        ValT&           at(const KeyT& k);
        ValT     const& at(const KeyT& k) const;
        ValT&           at(const KeyT& k,       ValT& defaultVal);
        ValT     const& at(const KeyT& k, const ValT& defaultVal) const;

        ValContT const& values() const;
        MapT     const& indices() const;
        KeyContT const& keys() const;
        IdxT            count(const KeyT& k) const;
        bool            has(const KeyT& k) const;
        void            clear();

        using iterator       = typename ValContT::iterator;
        using const_iterator = typename ValContT::const_iterator;

        iterator begin();
        iterator end();

        const_iterator begin() const;
        const_iterator end()   const;

        const_iterator cbegin() const;
        const_iterator cend()   const;


    private:
        template<class T, class = typename std::enable_if<meta::HasToString<T>::value>::type>
        static std::string DescribeKey(const T* key);
        static std::string DescribeKey(...);

        MapT indices_;
        KeyContT keys_;
        ValContT values_;
    };
}

// implementation
namespace armarx
{
    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    void KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::add(KeyT key, ValT value)
    {
        if (indices_.find(key) != indices_.end())
        {
            throw std::invalid_argument {"Already added a value for this key" + DescribeKey(&key)};
        }
        indices_.emplace(key, size());
        values_.emplace_back(std::move(value));
        keys_.emplace_back(std::move(key));
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    IdxT KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::index(const KeyT& k) const
    {
        return indices_.at(k);
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    ValT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::at(IdxT i)
    {
        return values_.at(i);
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    const ValT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::at(IdxT i) const
    {
        return values_.at(i);
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    ValT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::at(const KeyT& k)
    {
        return at(index(k));
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    const ValT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::at(const KeyT& k) const
    {
        return at(index(k));
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    ValT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::at(const KeyT& k, ValT& defaultVal)
    {
        return has(k) ? at(index(k)) : defaultVal;
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    const ValT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::at(const KeyT& k, const ValT& defaultVal) const
    {
        return has(k) ? at(index(k)) : defaultVal;
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    IdxT KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::size() const
    {
        return values_.size();
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    const ValContT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::values() const
    {
        return values_;
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    const MapT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::indices() const
    {
        return indices_;
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    const KeyContT& KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::keys() const
    {
        return keys_;
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    IdxT KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::count(const KeyT& k) const
    {
        return indices_.count(k);
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    bool KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::has(const KeyT& k) const
    {
        return indices_.count(k);
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    void KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::clear()
    {
        indices_.clear();
        keys_.clear();
        values_.clear();
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    typename KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::iterator
    KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::begin()
    {
        return values_.begin();
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    typename KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::iterator
    KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::end()
    {
        return values_.end();
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    typename KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::const_iterator
    KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::begin() const
    {
        return values_.cbegin();
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    typename KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::const_iterator
    KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::end() const
    {
        return values_.cend();
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    typename KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::const_iterator
    KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::cbegin() const
    {
        return begin();
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    typename KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::const_iterator
    KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::cend() const
    {
        return end();
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    std::string KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::DescribeKey(...)
    {
        return "";
    }

    template < class KeyT, class ValT, class KeyContT, class ValContT, class IdxT,  class MapT>
    template<class T, class>
    std::string KeyValueVector<KeyT, ValT, KeyContT, ValContT, IdxT, MapT>::DescribeKey(const T* key)
    {
        return ": " + to_string(*key);
    }
}
