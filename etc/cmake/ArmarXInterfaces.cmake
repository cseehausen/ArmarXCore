# Macros for ArmarX Framework Interfaces

#######################################################################################
# ICE INTERFACES
#######################################################################################

# remove ".ice" suffix
macro(stripIceSuffix SLICE_FILE STRIPPED_SLICE_FILE)
    string(LENGTH ${SLICE_FILE} LENGTH)
    math(EXPR LENGTH "${LENGTH} - 4")
    string(SUBSTRING ${SLICE_FILE} 0 ${LENGTH} STRIPPED_SLICE_FILE)
endmacro()

# generate a list of dependencies from SLICE_FILE to other slice files
# return the list in SLICE_CPP_DEPENDS
macro(sliceDependencies SLICE_CURRENT_CPP_FLAGS SLICE_FILE SLICE_CPP_DEPENDS)
    set(CPP_DEPEND_FILE ${CMAKE_BINARY_DIR}/slice_cpp_depend.txt)

    execute_process(COMMAND ${Ice_slice2cpp} ${SLICE_CURRENT_CPP_FLAGS} "${SLICE_FILE}" --depend
                    OUTPUT_FILE ${CPP_DEPEND_FILE}
                    ERROR_QUIET
                    ERROR_VARIABLE SLICE2CPP_STDERR
                    RESULT_VARIABLE SLICE2CPP_RESULT)

    if (${SLICE2CPP_RESULT})
        # If there was an error (result != 0), print it to stderr via CMake warning.
        message(WARNING "${SLICE2CPP_STDERR}")
    elseif (NOT "${SLICE2CPP_STDERR}" STREQUAL "")
        # If there was no error, the output in stderr can be considered a warning.  Write to log.
        STRING(REPLACE ";" " " SLICE_COMMAND_FLAGS "${SLICE_CURRENT_CPP_FLAGS}")
        SET(SLICE_COMMAND "${Ice_slice2cpp} ${SLICE_COMMAND_FLAGS} \"${SLICE_FILE}\"")
        file(APPEND "${CMAKE_BINARY_DIR}/slice_cpp_warnings.log" "WARNINGS EXECUTING THE FOLLOWING COMMAND:\n\n${SLICE_COMMAND}\n\n")
        file(APPEND "${CMAKE_BINARY_DIR}/slice_cpp_warnings.log" "${SLICE2CPP_STDERR}")
        file(APPEND "${CMAKE_BINARY_DIR}/slice_cpp_warnings.log" "\n----------\n\n")
    endif()

    # parse the file into a list of strings
    file(STRINGS ${CPP_DEPEND_FILE} SLICE_CPP_DEPENDS_DIRTY)
    # IMPORTANT: this magic line is required, no idea why.
    # without it, 'slice_depends' is not a list, has only one element.
    set(SLICE_CPP_DEPENDS_DIRTY ${SLICE_CPP_DEPENDS_DIRTY})
    list( LENGTH SLICE_CPP_DEPENDS_DIRTY listlen )
    if (${listlen} GREATER 0)
        # the first element is the generated file, we remove it.
        list(REMOVE_AT SLICE_CPP_DEPENDS_DIRTY 0)
    else()
        MESSAGE("WARNING: Empty dependency file created with CURRENT_SLICE_FILE=${CURRENT_SLICE_FILE}")
    endif()

    # the rest are the .ice file dependencies. there's at least one: the actual
    # source .ice file. all dependencies end up having a leading "\ "
    # if we don't remove them, add_custom_command() will interpret them as relative commands
    foreach(SLICE_CPP_DEPEND ${SLICE_CPP_DEPENDS_DIRTY})
        string(LENGTH ${SLICE_CPP_DEPEND} SLICE_CPP_DEPEND_LENGTH)
        math(EXPR SLICE_CPP_DEPEND_LENGTH "${SLICE_CPP_DEPEND_LENGTH}-1")
        string(SUBSTRING ${SLICE_CPP_DEPEND} 1 ${SLICE_CPP_DEPEND_LENGTH} SLICE_CPP_DEPEND)
        string(STRIP ${SLICE_CPP_DEPEND} SLICE_CPP_DEPEND)
        list(APPEND SLICE_CPP_DEPENDS ${SLICE_CPP_DEPEND})
    endforeach()
endmacro()

# set up flags required by slice2java and add the javainterface target
macro(generateJavaInterface)
    set(SLICE2JAVA_FLAGS -noinput)
    list(APPEND SLICE2JAVA_FLAGS -buildfile "${ArmarXCore_TEMPLATES_DIR}/buildJavaInterfaces.xml")
    list(APPEND SLICE2JAVA_FLAGS -DIce.slicedir="${Ice_Slice_DIR}")
    list(APPEND SLICE2JAVA_FLAGS -DIce.javalibdir="${Ice_Java_DIR}")
    list(APPEND SLICE2JAVA_FLAGS -Ddepend.interfaces="${SLICE2JAVA_DEPEND_INTERFACES}")
    list(APPEND SLICE2JAVA_FLAGS -Ddepend.jars="${SLICE2JAVA_DEPEND_JARS}")
    list(APPEND SLICE2JAVA_FLAGS -Djar.name="${LIB_NAME}")
    list(APPEND SLICE2JAVA_FLAGS -Dbasedir="${PROJECT_SOURCECODE_DIR}")
    list(APPEND SLICE2JAVA_FLAGS -Dgenerate.dir="${PROJECT_BINARY_DIR}/interface/java/")
    list(APPEND SLICE2JAVA_FLAGS -Doutput.dir="${CMAKE_BINARY_DIR}")
    if (NOT ${VERBOSE})
        list(APPEND SLICE2JAVA_FLAGS -quiet)
    endif()
    add_custom_target(javainterface
                      COMMAND ant ${SLICE2JAVA_FLAGS}
                      WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
                      COMMENT "Building Java Interface file")
    jar_install("${LIB_NAME}.jar")
endmacro()


macro(armarx_interfaces_generate_library_collect_dependencies)
    foreach(p ${ARGN})
        list(APPEND DEPENDEND_PROJECT_NAMES ${${p}_INTERFACE_DEPENDENCIES})
        armarx_interfaces_generate_library_collect_dependencies(${${p}_INTERFACE_DEPENDENCIES})
    endforeach()
endmacro()

# This macro uses the slice compiler from ice to generate an interface
# library which must be linked against when deriving from any of the
# interfaces compiled into the interface library.
#
# Output: interface library with name lib${NAME}Interfaces
#
# Before calling this macro the SLICE_FILES variable needs to be set
# and contain all required slice files
#
# Required arguments:
#   NAME
#
# Optional argument:
#   2nd Argument (DEPENDEND_PROJECT_NAMES): list of projects the interfaces depen on
#                                      (e.g. ArmarXCore if ${ArmarXCore_INTERFACE_DIRS}
#                                       needs to be added to the slice search paths)
#
macro(armarx_interfaces_generate_library NAME)
    # interface library version
    set(LIB_NAME       ${NAME}Interfaces)
    armarx_set_target("Interfaces: ${LIB_NAME}")

    # used in Installation.cmake
    set(ARMARX_PROJECT_INTERFACE_LIBRARY "${LIB_NAME}" CACHE INTERNAL "" FORCE)
    set("${NAME}_INTERFACE_LIBRARY" "${LIB_NAME}" CACHE INTERNAL "" FORCE)

    set(DEPENDEND_PROJECT_NAMES "${ARGV1}")
    set(PROJECT_INTERFACE_DEPENDENCIES "${DEPENDEND_PROJECT_NAMES}" CACHE INTERNAL "" FORCE)
    set("${NAME}_INTERFACE_DEPENDENCIES" "${DEPENDEND_PROJECT_NAMES}" CACHE INTERNAL "" FORCE)

    armarx_interfaces_generate_library_collect_dependencies(${DEPENDEND_PROJECT_NAMES})
    list(REMOVE_DUPLICATES DEPENDEND_PROJECT_NAMES)

    list(LENGTH DEPENDEND_PROJECT_NAMES PRINT_DEPENDENCIES)
    if (PRINT_DEPENDENCIES)
        message(STATUS "        Dependencies:")
        printlist("            " "${DEPENDEND_PROJECT_NAMES}")
    endif()

    # project vars
    set(SLICE_COMMON_FLAGS
        "-I${PROJECT_SOURCECODE_DIR}"
        "-I${Ice_Slice_DIR}"
        --underscore)

    set(SLICE2CPP_INPUT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
    set(SLICE2CPP_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}")

    # the uppercase string is needed for the --dll-export slice flag
    string(TOUPPER ${LIB_NAME} LIB_NAME_UPPERCASE)
    set(SLICE2CPP_FLAGS --dll-export "${LIB_NAME_UPPERCASE}_IMPORT_EXPORT")

    set(SLICE2JAVA_DEPEND_INTERFACES "")
    set(SLICE2JAVA_DEPEND_JARS "")

    set(SLICE_INCLUDE_FLAGS "")

    # project interface dependency paths
    # go through all dependent projects
    foreach(DEPENDEND_PROJECT_NAME ${DEPENDEND_PROJECT_NAMES})
        # append all directories found in <DEPENDEND_PROJECT_NAME>__INTERFACE_DIRS
        # as include directories for the current compilation process
        foreach(DEPENDEND_INTERFACE_DIR ${${DEPENDEND_PROJECT_NAME}_INTERFACE_DIRS})
            list(APPEND SLICE_INCLUDE_FLAGS "-I${DEPENDEND_INTERFACE_DIR}")
            set(SLICE2JAVA_DEPEND_INTERFACES "${SLICE2JAVA_DEPEND_INTERFACES}:${DEPENDEND_INTERFACE_DIR}")
        endforeach(DEPENDEND_INTERFACE_DIR)

        include_directories(${${DEPENDEND_PROJECT_NAME}_INCLUDE_DIRS})

        # append all dependent interface libraries to the DEPENDEND_INTERFACE_LIBRARIES
        # CMake variable which is used further down in target_link_libraries()
        list(APPEND DEPENDEND_INTERFACE_LIBRARIES "${${DEPENDEND_PROJECT_NAME}_INTERFACE_LIBRARY}")

        # build a list of all dependent java archives
        # needed by the slice2java compiler
        foreach(DEPENDEND_JAR ${${DEPENDEND_PROJECT_NAME}_JARS})
            set(SLICE2JAVA_DEPEND_JARS "${${DEPENDEND_PROJECT_NAME}_JAR_DIR}/${DEPENDEND_JAR}:${SLICE2JAVA_DEPEND_JARS}")
        endforeach(DEPENDEND_JAR)
    endforeach(DEPENDEND_PROJECT_NAME)

    list(APPEND SLICE_COMMON_FLAGS ${SLICE_INCLUDE_FLAGS})

    # setup C++ files in the following loop
    set(LIB_FILES "")
    set(LIB_HEADER_FILES "")

    # Clear file were ice warnings are written to.
    file(WRITE "${CMAKE_BINARY_DIR}/slice_cpp_warnings.log" "")

    # run slice on files
    foreach(CURRENT_SLICE_FILE ${SLICE_FILES})
        set(STRIPPED_SLICE_FILE "")
        stripIceSuffix(${CURRENT_SLICE_FILE} STRIPPED_SLICE_FILE)

        # File is usually from the scheme "some/dir/${CURRENT_SLICE_FILE}"
        # this REGEX matches against ${STRIPPED_SLICE_FILE} and
        # CMAKE_MATCH_1 contains "some/dir" afterwards
        string(REGEX MATCH "^(.+)/[^/]+$" VOID ${STRIPPED_SLICE_FILE})
        set(DIR ${CMAKE_MATCH_1})

        # put generated files in a directory with the same hierarchy and names as the .ice file
        set(SLICE_CURRENT_CPP_FLAGS ${SLICE_COMMON_FLAGS} --output-dir ${SLICE2CPP_OUTPUT_DIR}/${DIR})

        # Always add a prefix to the #include directives of generated files
        # Since only #include <> directives are generated the local directory is not
        # regarded when searching for matching header files which would require
        # adding additional include_directories.
        # $DIR is empty if REGEX couldn't match anything in ${STRIPPED_SLICE_FILE}
        # e.g. the file was not found in a subdirectory of CMAKE_CURRENT_SOURCE_DIR.
        if ("${DIR}" STREQUAL "")
            # omit double // in generated files if $DIR is empty
            list(APPEND SLICE_CURRENT_CPP_FLAGS --include-dir "${ARMARX_PROJECT_NAME}/interface")
        else()
            list(APPEND SLICE_CURRENT_CPP_FLAGS --include-dir "${ARMARX_PROJECT_NAME}/interface/${DIR}")
        endif()

        # Get the dependencies of the current slice file
        # required for the slice2cpp target
        set(SLICE_CPP_DEPENDS "")
        set(SLICE_INPUT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
        sliceDependencies("${SLICE_CURRENT_CPP_FLAGS}" "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}" SLICE_CPP_DEPENDS)
#        message(FATAL_ERROR  "slices: ${SLICE_CPP_DEPENDS}")
        # make sure the directory for generated files exists
        file(MAKE_DIRECTORY ${SLICE2CPP_OUTPUT_DIR}/${DIR})

        # generate the header and implementation files from the slice file
        add_custom_command(OUTPUT  ${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.h
                                   ${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.cpp
                           COMMAND ${Ice_slice2cpp}
                           ARGS    ${SLICE_CURRENT_CPP_FLAGS} ${SLICE2CPP_FLAGS} "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}"
                           DEPENDS ${SLICE_CPP_DEPENDS}
                           MAIN_DEPENDENCY "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}"
                           COMMENT "-- Generating cpp file from ${ARMARX_PROJECT_NAME}/interface/${CURRENT_SLICE_FILE}")

        list(APPEND LIB_FILES        ${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.cpp)
        list(APPEND LIB_HEADER_FILES ${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.h)
    endforeach()

    if(SLICE_FILES_ADDITIONAL_HEADERS)
        list(APPEND LIB_HEADER_FILES ${SLICE_FILES_ADDITIONAL_HEADERS})
    endif()

    if(SLICE_FILES_ADDITIONAL_SOURCES)
        list(APPEND LIB_FILES        ${SLICE_FILES_ADDITIONAL_SOURCES})
    endif()

    # custom target that is reached once all files have been generated
    add_custom_target(slice2cpp_FINISHED ALL
        DEPENDS ${LIB_FILES}
    )
    add_dependencies(all_generate slice2cpp_FINISHED)

    # BUILD_INTERFACE_LIBRARY evaluates to FALSE if length is 0
    list(LENGTH LIB_FILES BUILD_INTERFACE_LIBRARY)

    # only build the library if all files have been generated
    armarx_build_if(BUILD_INTERFACE_LIBRARY "NOT building interface library <${LIB_NAME}> because no Slice files could be found in: ${CMAKE_CURRENT_SOURCE_DIR}")
    if(${BUILD_INTERFACE_LIBRARY})
        list(APPEND DEPENDEND_INTERFACE_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
        armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADER_FILES}" "${DEPENDEND_INTERFACE_LIBRARIES};BoostAssertionHandler")
        if(COMPILER_SUPPORTS_suggest_override)
            target_compile_options(${LIB_NAME} PRIVATE -Wno-suggest-override)
        endif()
        add_dependencies(slice2cpp_FINISHED ${LIB_NAME})
    endif()

    set(SLICE2HTML_INPUT_FILES "")
    foreach(CURRENT_SLICE_FILE ${SLICE_FILES})
        list(APPEND SLICE2HTML_INPUT_FILES "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}")
    endforeach()
    generateSliceDocumentation("${SLICE2HTML_INPUT_FILES}" "${SLICE_INCLUDE_FLAGS}")

    generateJavaInterface()
endmacro()


# This is a modified version of armarx_interfaces_generate_library
# since the ArmarX provided macro is not flexible enough
function(armarx_add_component_interface_lib)
    message(STATUS "    Generating component interfaces")
    set(multi_param
        SLICE_FILES
        ICE_LIBS)
    set(single_param)
    set(flag_param)

    cmake_parse_arguments(AX "${flag_param}" "${single_param}" "${multi_param}" ${ARGN})
    if(AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${ARMARX_COMPONENT_NAME}: encountered unparsed arguments! ${AX_UNPARSED_ARGUMENTS}\nARGN = ${ARGN}")
    endif()

    # interface library version
    set(LIB_NAME       "${ARMARX_COMPONENT_NAME}Interfaces")
    armarx_set_target("Component interfaces: ${LIB_NAME}")

    # used in Installation.cmake
    set("${LIB_NAME}_INTERFACE_LIBRARY" "${LIB_NAME}" CACHE INTERNAL "" FORCE)

    set(DEPENDEND_PROJECT_NAMES "${AX_ICE_LIBS}")
    set("${LIB_NAME}_INTERFACE_DEPENDENCIES" "${DEPENDEND_PROJECT_NAMES}" CACHE INTERNAL "" FORCE)

    armarx_interfaces_generate_library_collect_dependencies("${DEPENDEND_PROJECT_NAMES}")
    list(REMOVE_DUPLICATES DEPENDEND_PROJECT_NAMES)

    list(LENGTH DEPENDEND_PROJECT_NAMES PRINT_DEPENDENCIES)
    if (PRINT_DEPENDENCIES)
        message(STATUS "        Dependencies:")
        printlist("            " "${DEPENDEND_PROJECT_NAMES}")
    endif()

    # project vars
    set(SLICE_COMMON_FLAGS
        "-I${PROJECT_SOURCECODE_DIR}"
        "-I${Ice_Slice_DIR}"
        #--stream
        --underscore)

    set(SLICE2CPP_INPUT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
    set(SLICE2CPP_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}")

    # the uppercase string is needed for the --dll-export slice flag
    string(TOUPPER ${LIB_NAME} LIB_NAME_UPPERCASE)
    string(REPLACE "-" "_" LIB_NAME_UPPERCASE "${LIB_NAME_UPPERCASE}")
    set(SLICE2CPP_FLAGS --dll-export "${LIB_NAME_UPPERCASE}_IMPORT_EXPORT")

    set(SLICE2JAVA_DEPEND_INTERFACES "")
    set(SLICE2JAVA_DEPEND_JARS "")

    set(SLICE_INCLUDE_FLAGS "")

    # project interface dependency paths
    # go through all dependent projects
    foreach(DEPENDEND_PROJECT_NAME ${DEPENDEND_PROJECT_NAMES})
        # append all directories found in <DEPENDEND_PROJECT_NAME>__INTERFACE_DIRS
        # as include directories for the current compilation process
        foreach(DEPENDEND_INTERFACE_DIR ${${DEPENDEND_PROJECT_NAME}_INTERFACE_DIRS})
            list(APPEND SLICE_INCLUDE_FLAGS "-I${DEPENDEND_INTERFACE_DIR}")
            set(SLICE2JAVA_DEPEND_INTERFACES "${SLICE2JAVA_DEPEND_INTERFACES}:${DEPENDEND_INTERFACE_DIR}")
        endforeach(DEPENDEND_INTERFACE_DIR)

        include_directories(${${DEPENDEND_PROJECT_NAME}_INCLUDE_DIRS})

        # append all dependent interface libraries to the DEPENDEND_INTERFACE_LIBRARIES
        # CMake variable which is used further down in target_link_libraries()
        list(APPEND DEPENDEND_INTERFACE_LIBRARIES "${${DEPENDEND_PROJECT_NAME}_INTERFACE_LIBRARY}")

        # build a list of all dependent java archives
        # needed by the slice2java compiler
        foreach(DEPENDEND_JAR ${${DEPENDEND_PROJECT_NAME}_JARS})
            set(SLICE2JAVA_DEPEND_JARS "${${DEPENDEND_PROJECT_NAME}_JAR_DIR}/${DEPENDEND_JAR}:${SLICE2JAVA_DEPEND_JARS}")
        endforeach(DEPENDEND_JAR)
    endforeach(DEPENDEND_PROJECT_NAME)

    list(APPEND SLICE_COMMON_FLAGS ${SLICE_INCLUDE_FLAGS})

    # setup C++ files in the following loop
    set(LIB_FILES "")
    set(LIB_HEADER_FILES "")

    # run slice on files
    foreach(CURRENT_SLICE_FILE ${AX_SLICE_FILES})
        set(STRIPPED_SLICE_FILE "")
        stripIceSuffix(${CURRENT_SLICE_FILE} STRIPPED_SLICE_FILE)

        # File is usually from the scheme "some/dir/${CURRENT_SLICE_FILE}"
        # this REGEX matches against ${STRIPPED_SLICE_FILE} and
        # CMAKE_MATCH_1 contains "some/dir" afterwards
        string(REGEX MATCH "^(.+)/[^/]+$" VOID ${STRIPPED_SLICE_FILE})
        set(DIR ${CMAKE_MATCH_1})

        # put generated files in a directory with the same hierarchy and names as the .ice file
        set(SLICE_CURRENT_CPP_FLAGS ${SLICE_COMMON_FLAGS} --output-dir ${SLICE2CPP_OUTPUT_DIR}/${DIR})

        # Always add a prefix to the #include directives of generated files
        # Since only #include <> directives are generated the local directory is not
        # regarded when searching for matching header files which would require
        # adding additional include_directories.
        # $DIR is empty if REGEX couldn't match anything in ${STRIPPED_SLICE_FILE}
        # e.g. the file was not found in a subdirectory of CMAKE_CURRENT_SOURCE_DIR.
        string(LENGTH "${PROJECT_SOURCE_DIR}/source/" PROJECT_SOURCE_DIR_LEN)
        string(SUBSTRING "${CMAKE_CURRENT_SOURCE_DIR}" "${PROJECT_SOURCE_DIR_LEN}" "-1" SLICE_FILE_INCLUDE_DIR)
        #set(SLICE_FILE_INCLUDE_DIR "")
        if ("${DIR}" STREQUAL "")
            # omit double // in generated files if $DIR is empty
            list(APPEND SLICE_CURRENT_CPP_FLAGS --include-dir "${SLICE_FILE_INCLUDE_DIR}")
        else()
            list(APPEND SLICE_CURRENT_CPP_FLAGS --include-dir "${SLICE_FILE_INCLUDE_DIR}/${DIR}")
        endif()

        # Get the dependencies of the current slice file
        # required for the slice2cpp target
        set(SLICE_CPP_DEPENDS "")
        set(SLICE_INPUT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
        sliceDependencies("${SLICE_CURRENT_CPP_FLAGS}" "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}" SLICE_CPP_DEPENDS)
#        message(FATAL_ERROR  "slices: ${SLICE_CPP_DEPENDS}")
        # make sure the directory for generated files exists
        file(MAKE_DIRECTORY ${SLICE2CPP_OUTPUT_DIR}/${DIR})

        # generate the header and implementation files from the slice file
        add_custom_command(OUTPUT  ${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.h
                                   ${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.cpp
                           COMMAND ${Ice_slice2cpp}
                           ARGS    ${SLICE_CURRENT_CPP_FLAGS} ${SLICE2CPP_FLAGS} "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}"
                           DEPENDS ${SLICE_CPP_DEPENDS}
                           MAIN_DEPENDENCY "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}"
                           COMMENT "-- Generating cpp file from ${ARMARX_PROJECT_NAME}/interfaces/${CURRENT_SLICE_FILE}")

        list(APPEND LIB_FILES        ${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.cpp)
        list(APPEND LIB_HEADER_FILES ${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.h)
    endforeach()

    if(SLICE_FILES_ADDITIONAL_HEADERS)
        list(APPEND LIB_HEADER_FILES ${SLICE_FILES_ADDITIONAL_HEADERS})
    endif()

    if(SLICE_FILES_ADDITIONAL_SOURCES)
        list(APPEND LIB_FILES        ${SLICE_FILES_ADDITIONAL_SOURCES})
    endif()

    # BUILD_INTERFACE_LIBRARY evaluates to FALSE if length is 0
    list(LENGTH LIB_FILES BUILD_INTERFACE_LIBRARY)

    # only build the library if all files have been generated
    armarx_build_if(BUILD_INTERFACE_LIBRARY "NOT building interface library <${LIB_NAME}> because no Slice files could be found in: ${CMAKE_CURRENT_SOURCE_DIR}")
    if(${BUILD_INTERFACE_LIBRARY})
        list(APPEND DEPENDEND_INTERFACE_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
        armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADER_FILES}" "${DEPENDEND_INTERFACE_LIBRARIES}")
        #target_sources("${LIB_NAME}" PRIVATE ${LIB_FILES} ${LIB_HEADER_FILES})
        if(COMPILER_SUPPORTS_suggest_override)
            target_compile_options("${LIB_NAME}" PRIVATE -Wno-suggest-override)
        endif()
    endif()

    set(SLICE2HTML_INPUT_FILES "")
    foreach(CURRENT_SLICE_FILE ${SLICE_FILES})
        list(APPEND SLICE2HTML_INPUT_FILES "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}")
    endforeach()
    #generateSliceDocumentation("${SLICE2HTML_INPUT_FILES}" "${SLICE_INCLUDE_FLAGS}")

    #generateJavaInterface()
endfunction()


#######################################################################################
# ARON INTERFACES
#######################################################################################

# remove ".xml" suffix
macro(stripXMLSuffix XML_FILE STRIPPED_XML_FILE)
    string(LENGTH ${XML_FILE} LENGTH)
    math(EXPR LENGTH "${LENGTH} - 4")
    string(SUBSTRING ${XML_FILE} 0 ${LENGTH} STRIPPED_XML_FILE)
endmacro()

# function to add the aron code generation. Must be called after(!!) the generation of TARGET_NAME (e.g. using armarx_add_test(...))
function(armarx_enable_aron_file_generation_for_target)
    message(STATUS "    Adding ArmarX Aron File Generation step to make")
    set(multi_param
        ARON_FILES
    )
    set(single_param
        TARGET_NAME
    )
    set(flag_param
        ENABLE_DEBUG_INFO
    )

    cmake_parse_arguments(AX "${flag_param}" "${single_param}" "${multi_param}" ${ARGN})
    if(AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${ARMARX_COMPONENT_NAME}: encountered unparsed arguments! ${AX_UNPARSED_ARGUMENTS}\nARGN = ${ARGN}")
    endif()

    if(TARGET ${AX_TARGET_NAME})
    
        set(ARON_ADDITIONAL_ARGUMENTS "")
        if(${AX_ENABLE_DEBUG_INFO})
            set(ARON_ADDITIONAL_ARGUMENTS "-v")
        endif()

        foreach(CURRENT_ARON_XML_FILE ${AX_ARON_FILES})

            #file(MAKE_DIRECTORY "/tmp/armarxcmake-$ENV{USER}/")
            #file(MAKE_DIRECTORY "/tmp/armarxcmake-$ENV{USER}/aron/")
            #configure_file(${CURRENT_ARON_XML_FILE} "/tmp/armarxcmake-$ENV{USER}/aron/${CURRENT_ARON_XML_FILE}")

            get_filename_component(ARON_INPUT_FILENAME "${CURRENT_ARON_XML_FILE}" NAME)

            set(ARON_INPUT_FILE "${CMAKE_CURRENT_SOURCE_DIR}/${CURRENT_ARON_XML_FILE}")
            set(ARON_OUTPUT_FOLDER "${CMAKE_CURRENT_BINARY_DIR}/aron/")
            #string(REPLACE ".xml" ".aron.generated.h" ARON_OUTPUT_FILENAME "${ARON_INPUT_FILENAME}")
            #set(ARON_OUTPUT_FILE "${ARON_OUTPUT_FOLDER}/${ARON_OUTPUT_FILENAME}")
            file(MAKE_DIRECTORY ${ARON_OUTPUT_FOLDER})

            message("        Adding custom aron generation target for file: ${ARON_INPUT_FILE}")
            #message("        Expected output: ${ARON_OUTPUT_FILE}")

            #message("        Generating file in folder: ${CMAKE_CURRENT_BINARY_DIR}")

            #set(ARON_INPUT_FILE_GENERATION_NAME "${ARON_INPUT_FILE}/Generate_Aron_Header")
            set(ARON_CURRENT_TARGET_NAME "${AX_TARGET_NAME}_GENERATE_ARON_FILES_${ARON_INPUT_FILENAME}")

            add_custom_target(
                "${ARON_CURRENT_TARGET_NAME}"
                COMMAND "${RobotAPI_BINARY_DIR}/AronCodeGeneratorAppRun" "-i" "${ARON_INPUT_FILE}" "-o" "${ARON_OUTPUT_FOLDER}" "${ARON_ADDITIONAL_ARGUMENTS}"
                DEPENDS AronCodeGeneratorAppRun
            )
            add_dependencies("${AX_TARGET_NAME}" "${ARON_CURRENT_TARGET_NAME}")
        endforeach()
        add_custom_target("${AX_TARGET_NAME}_AronFiles" SOURCES ${AX_ARON_FILES})
    else()
        message("        WARNING! Target '${AX_TARGET_NAME}' not found. Continue without generating aron files.")
    endif()

endfunction()
