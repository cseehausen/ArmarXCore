/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <string>
#include <set>

namespace armarx
{
    class DefaultRapidXmlReaderNode
    {
    private:
        std::vector<RapidXmlReaderNode> nodes;

        void check() const
        {
            if (nodes.empty())
            {
                throw exceptions::local::RapidXmlReaderException("DefaultRapidXmlWrapper NullPointerException");
            }
        }

        std::string get_attrib_value(const char* attrName) const
        {
            check();
            for (auto& node : nodes)
            {
                if (node.has_attribute(attrName))
                {
                    return node.attribute_value(attrName);
                }
            }

            //build error
            std::stringstream ch;
            ch << "\nchildren " << getChildPaths();
            throw exceptions::local::RapidXmlReaderException(
                std::string("Attribute '") + attrName +
                "' does not exist in node " + getPathsString() + ch.str());
        }

        std::string get_value() const
        {
            RapidXmlReaderNode n = nodes.front();
            return n.value();
        }

    public:
        DefaultRapidXmlReaderNode() = default;
        DefaultRapidXmlReaderNode(DefaultRapidXmlReaderNode&&) = default;
        DefaultRapidXmlReaderNode(DefaultRapidXmlReaderNode const&) = default;
        DefaultRapidXmlReaderNode& operator=(DefaultRapidXmlReaderNode&&) = default;
        DefaultRapidXmlReaderNode& operator=(DefaultRapidXmlReaderNode const&) = default;

        DefaultRapidXmlReaderNode(std::vector<RapidXmlReaderNode> rnodes)
        {
            for (RapidXmlReaderNode& n : rnodes)
            {
                if (!n.is_valid())
                {
                    throw exceptions::local::RapidXmlReaderException("DefaultRapidXmlWrapper NullPointerException in constructor");
                }
            }
            reverse(rnodes.begin(), rnodes.end());
            nodes = std::move(rnodes);
        }
        DefaultRapidXmlReaderNode(const RapidXmlReaderNode& n) :
            DefaultRapidXmlReaderNode(std::vector{n})
        {}

        DefaultRapidXmlReaderNode first_node(const std::string& name) const
        {
            return first_node(name.c_str());
        }
        DefaultRapidXmlReaderNode first_node(const char* name = nullptr) const
        {
            check();
            std::vector<RapidXmlReaderNode> v;
            for (const RapidXmlReaderNode& n : nodes)
            {
                if (n.get_node_ptr()->first_node(name) != nullptr)
                {
                    v.push_back(n.first_node(name));
                }
            }
            if (v.empty())
            {
                std::stringstream ch;
                ch << "\nchildren " << getChildPaths();
                throw exceptions::local::RapidXmlReaderException(
                    std::string("Node with name '") + name +
                    "' does not exist in node " + getPathsString() + ch.str());
            }
            reverse(v.begin(), v.end());
            return DefaultRapidXmlReaderNode(std::move(v));
        }

        bool has_attribute(const std::string& attrName) const
        {
            return has_attribute(attrName.c_str());
        }
        bool has_attribute(const char* attrName) const
        {
            check();
            for (RapidXmlReaderNode n : nodes)
            {
                if (n.has_attribute(attrName))
                {
                    return true;
                }
            }
            return false;
        }

        bool attribute_as_bool(const std::string& attrName, const std::string& trueValue, const std::string& falseValue) const
        {
            return attribute_as_bool(attrName.c_str(), trueValue, falseValue);
        }
        bool attribute_as_bool(const char* attrName, const std::string& trueValue, const std::string& falseValue) const
        {
            std::string value = std::string(get_attrib_value(attrName));

            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                throw exceptions::local::RapidXmlReaderException(std::string("Invalid value '") + value + "' for attribute '" + attrName + "'. Expecting '" + trueValue + "' or '" + falseValue + "'.");
            }
        }

        bool attribute_as_optional_bool(const std::string& name, const std::string& trueValue, const std::string& falseValue, bool defaultValue) const
        {
            return attribute_as_optional_bool(name.c_str(), trueValue, falseValue, defaultValue);
        }
        bool attribute_as_optional_bool(const char* name, const std::string& trueValue, const std::string& falseValue, bool defaultValue) const
        {
            check();

            if (!has_attribute(name))
            {
                return defaultValue;
            }

            std::string value = std::string(get_attrib_value(name));

            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                throw exceptions::local::RapidXmlReaderException(std::string("Invalid value '") + value + "' for attribute '" + name + "'. Expecting '" + trueValue + "' or '" + falseValue + "'.");
            }
        }

        std::string attribute_as_string(const std::string& attrName) const
        {
            return attribute_as_string(attrName.c_str());
        }
        std::string attribute_as_string(const char* attrName) const
        {
            return std::string(get_attrib_value(attrName));
        }

        template<class T> void attribute_as(const std::string& attrName, T& value) const
        {
            return attribute_as(attrName.c_str(), value);
        }
        template<class T> void attribute_as(const char* attrName, T& value) const
        {
            std::stringstream(get_attrib_value(attrName)) >> value;
        }

        template<class T> T attribute_as(const std::string& attrName) const
        {
            return attribute_as<T>(attrName.c_str());
        }
        template<class T> T attribute_as(const char* attrName) const
        {
            T val;
            attribute_as(attrName, val);
            return val;
        }

        float attribute_as_float(const std::string& attrName) const
        {
            return attribute_as_float(attrName.c_str());
        }
        float attribute_as_float(const char* attrName) const
        {
            return attribute_as<float>(attrName);
        }

        uint32_t attribute_as_uint(const std::string& attrName) const
        {
            return attribute_as_uint(attrName.c_str());
        }
        uint32_t attribute_as_uint(const char* attrName) const
        {
            return attribute_as<uint32_t>(attrName);
        }

        std::string value_as_string() const
        {
            check();
            return std::string(get_value());
        }

        float value_as_float() const
        {
            check();
            std::stringstream strValue(get_value());
            float retValue;
            strValue >> retValue;
            return retValue;
        }

        uint32_t value_as_uint32() const
        {
            check();
            std::stringstream strValue(get_value());
            uint32_t retValue;
            strValue >> retValue;
            return retValue;
        }

        int32_t value_as_int32() const
        {
            check();
            std::stringstream strValue(get_value());
            int32_t retValue;
            strValue >> retValue;
            return retValue;
        }

        bool value_as_bool(const std::string& trueValue, const std::string& falseValue) const
        {
            check();
            std::string value = std::string(get_value());
            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                std::string s = "(";
                for (RapidXmlReaderNode node : nodes)
                {
                    s += std::string(node.name());
                    s += ", ";
                }
                s = s.substr(0, s.length() - 2);
                s += ")";
                throw exceptions::local::RapidXmlReaderException(std::string("Invalid value '") + value + "' for represented nodes " + s + ". Expecting '" + trueValue + "' or '" + falseValue + "'.");
            }
        }

        bool value_as_optional_bool(const std::string& trueValue, const std::string& falseValue, bool defaultValue) const
        {
            check();
            std::string value = std::string(get_value());
            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                return defaultValue;
            }
        }

        bool is_valid() const
        {
            return !nodes.empty();
        }

        std::vector<std::string> getChildPaths() const
        {
            check();
            std::set<std::string> results;
            for (const RapidXmlReaderNode& n : nodes)
            {
                const auto paths = n.getChildPaths();
                results.insert(paths.begin(), paths.end());
            }
            return {results.begin(), results.end()};
        }
        std::vector<std::string> getPaths() const
        {
            check();
            std::vector<std::string> results;
            for (const RapidXmlReaderNode& n : nodes)
            {
                results.push_back(n.getPath());
            }
            return results;
        }
        std::string getPathsString() const
        {
            std::vector<std::string> paths = getPaths();
            std::string all_paths;
            for (std::string path : paths)
            {
                all_paths += path;
                all_paths += " or ";
            }
            all_paths = all_paths.substr(0, all_paths.length() - 4);
            return all_paths;
        }


        DefaultRapidXmlReaderNode add_node_at_end(RapidXmlReaderNode node)
        {
            return add_node_at(node, nodes.size());
        }
        DefaultRapidXmlReaderNode add_node_at_end(DefaultRapidXmlReaderNode node)
        {
            return add_node_at(node.nodes, nodes.size());
        }

        DefaultRapidXmlReaderNode add_node_at(std::vector<RapidXmlReaderNode> added_nodes, size_t position)
        {
            if (position > nodes.size())
            {
                throw exceptions::local::RapidXmlReaderException("The index is out of range: given index ") << position << " size of container: " << nodes.size();
            }

            if (!added_nodes.empty())
            {
                std::vector<RapidXmlReaderNode> v;
                v.reserve(added_nodes.size() + nodes.size());
                for (auto& node : added_nodes)
                {
                    if (node.is_valid())
                    {
                        v.emplace_back(std::move(node));
                    }
                }
                if (!v.empty())
                {
                    const auto n_inserted = v.size();
                    v.insert(v.end(), nodes.rbegin(), nodes.rend());
                    ARMARX_CHECK_EQUAL(n_inserted + nodes.size(), v.size());
                    auto it = v.begin();
                    std::rotate(it, it + n_inserted, it + n_inserted + position);
                    return DefaultRapidXmlReaderNode(std::move(v));
                }
            }
            return *this;
        }
        DefaultRapidXmlReaderNode add_node_at(RapidXmlReaderNode node, size_t position)
        {
            return add_node_at(std::vector{node}, position);
        }

        DefaultRapidXmlReaderNode remove_node_at(size_t pos)
        {
            if (pos >= nodes.size())
            {
                pos = nodes.size() - 1;
            }
            if (is_valid())
            {
                std::vector<RapidXmlReaderNode> v = nodes;
                reverse(v.begin(), v.end());
                v.erase(v.begin() + pos);
                return DefaultRapidXmlReaderNode(std::move(v));
            }
            return *this;
        }

    };
}


