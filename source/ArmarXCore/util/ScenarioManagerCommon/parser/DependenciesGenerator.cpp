/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "DependenciesGenerator.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <filesystem>
#include <fstream>


using namespace ScenarioManager;
using namespace Parser;
using namespace Data_Structure;
using namespace armarx;

std::map<std::string, DependencyTree> DependenciesGenerator::cachedTrees;

DependenciesGenerator::DependenciesGenerator()
{
}

DependencyTree DependenciesGenerator::getDependencieTree(std::string packageName)
{
    if (cachedTrees.count(packageName))
    {
        return cachedTrees[packageName];
    }
    CMakePackageFinder parser = CMakePackageFinderCache::GlobalCache.findPackage(packageName);

    std::vector<std::string> dependencies = parser.getDependencies();
    std::vector<std::string> depsConcat = dependencies;

    if (!dependencies.empty() && !dependencies.at(0).empty())
    {
        for (auto package : dependencies)
        {
            std::vector<std::string> deps = getDependencieTree(package);
            for (auto dep : deps)
            {
                if (std::find(depsConcat.begin(), depsConcat.end(), dep) == depsConcat.end())
                {
                    depsConcat.push_back(dep);
                }
            }
        }
    }
    else
    {
        dependencies.clear();
        dependencies.push_back(packageName);
        cachedTrees[packageName] = dependencies;
        return dependencies;
    }

    depsConcat.push_back(packageName);

    cachedTrees[packageName] = depsConcat;

    return depsConcat;
}

void DependenciesGenerator::generateDependenciesCfg(PackagePtr package)
{
    if (package.get() == nullptr)
    {
        ARMARX_WARNING_S << "Unable to generate Dependencies cfg for unknown package";
    }
    std::filesystem::path cacheFolderPath = std::filesystem::path(ArmarXDataPath::GetCachePath()) / "ComponentFiles";

    if (!std::filesystem::exists(cacheFolderPath))
    {
        std::filesystem::create_directories(cacheFolderPath);
        // return value of create_directories is buggy ?! check again
        if (!std::filesystem::exists(cacheFolderPath))
        {
            ARMARX_ERROR_S << "Could not create Cache folder for ScenarioManagerPlugin at " << cacheFolderPath.string();
        }
    }

    std::filesystem::path cacheFilePath = cacheFolderPath / std::filesystem::path("./" + package->getName() + ".dependencies.cfg");

    std::ofstream out(cacheFilePath.string());

    out << "ArmarX.ProjectName=" << package->getName() << std::endl;

    DependencyTree deps = getDependencieTree(package->getName());
    std::string dataPaths = "";
    std::string depsString = "";

    size_t pos = 0;
    for (auto dep : deps)
    {
        CMakePackageFinder pFinder = CMakePackageFinderCache::GlobalCache.findPackage(dep);
        dataPaths += pFinder.getDataDir();
        depsString += dep;

        if (pos != deps.size() - 1)
        {
            dataPaths += ";";
            depsString += ";";
        }

        ++pos;
    }

    out << "ArmarX.ProjectDatapath=" << dataPaths << std::endl;
    out << "ArmarX.ProjectDependencies=" << depsString << std::endl;

    out.close();
}
