/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2019, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <functional>

// Ice
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>


namespace armarx
{
    /**
     * @brief Measures the passed time between the construction or calling `reset()` and `stop()`.
     *
     * The `StopWatch` uses the system time by default, but it may use the virtual time provided
     * by the time server as well.  Also has a static method `measure()`, which takes a lambda, and
     * returns the time it took to execute that lambda.
     *
     * Code examples:
     *
     * ```cpp
     * // By supplied methods.
     * StopWatch sw;
     * long_operation();
     * IceUtil::Time duration = sw.stop();
     * std::cout << "Operation took " << duration.toMilliSeconds() << " ms";
     * ```
     *
     * ```cpp
     * // By executing a lambda.
     * IceUtil::Time duration = StopWatch::assess([&]() {
     *     long_operation();
     * });
     * std::cout << "Operation took " << duration.toMilliSeconds() << " ms";
     * ```
     */
    class StopWatch
    {

    public:

        /**
         * @brief StopWatch Constructs a `StopWatch` and starts it immediately.
         * @param timeMode Time mode, uses system time as default.
         */
        StopWatch(armarx::TimeMode timeMode = armarx::TimeMode::SystemTime);

        /**
         * @brief Destructs the `StopWatch`.
         */
        virtual ~StopWatch();

        /**
         * @brief Measures the time needed to execute the given lambda and returns it.
         * @param subjectToMeasure Lambda to be measured
         * @param timeMode Time mode, uses system time as default.
         * @return Time it took to execute the given lambda.
         */
        static
        IceUtil::Time
        measure(
            std::function<void(void)> subjectToMeasure,
            armarx::TimeMode timeMode = armarx::TimeMode::SystemTime
        );

        /**
         * @brief Stops the timer and returns the measured time.
         * @return Time elapsed since construction or last call of `reset()`.
         */
        IceUtil::Time stop();

        /**
         * @brief Resets the timer.
         */
        void reset();

        /**
         * @brief Returns whether the timer is stopped or is actively measuring time.
         * @return True of timer is stopped, false otherwise.
         */
        bool isStopped() const;

        /**
         * @brief Returns the time at starting the timer.
         * @return Time at starting the timer.
         */
        IceUtil::Time startingTime() const;

        /**
         * @brief Returns the time at stopping the timer.
         * @return Time at stopping the timer.
         *
         * @throw std::logic_error When the timer was not stopped yet.
         */
        IceUtil::Time stoppingTime() const;

    private:

        armarx::TimeMode m_time_mode;

        IceUtil::Time m_starting_time;

        IceUtil::Time m_time;

    };
}
